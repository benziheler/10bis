//
//  SPDictionary.m
//  Speclet
//
//  Created by Vadim Pavlov on 30.10.11.
//  Copyright (c) 2011 HaskiTeam. All rights reserved.
//

#import "URLDictionary.h"

static inline NSString * CreateKeyValuePair (NSString * key, NSString * value) {
    
    return [NSString stringWithFormat:@"%@=%@", key, value];
}


@interface URLDictionary ()
@property (nonatomic, retain) NSMutableDictionary *dictionary;
@end

@implementation URLDictionary
@synthesize dictionary = dictionary_;

+ (id)dictionaryWithCapacity:(NSInteger)itemsNum;
{
    URLDictionary *newDict = [[URLDictionary alloc] initWithCapacity: itemsNum];
    return newDict;
}
- (id)init
{
    self = [super init];
    if  (self)
    {
        self.dictionary = [NSMutableDictionary dictionary];
    }
    return self;
}
- (NSDictionary*)dictionary
{
    return dictionary_;
}

- (id)initWithCapacity:(NSInteger)itemsNum
{
    self = [super init];
    if  (self)
    {
        self.dictionary = [NSMutableDictionary dictionaryWithCapacity: itemsNum];
    }
    
    return self;
}

- (void)setObject:(id)anObject forKey:(id)aKey
{
    if (anObject) 
    {
        [dictionary_ setObject: anObject forKey: aKey];
    }
}
- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key
{
    if (obj)
    {
        [dictionary_ setObject: obj forKeyedSubscript: key];
    }
}
- (void)removeObjectForKey:(id)aKey
{
    [dictionary_ removeObjectForKey: aKey];
}
- (NSUInteger)count
{
    return [self.dictionary count];
}
- (id)objectForKey:(id)aKey
{
    return  [self.dictionary objectForKey: aKey];
}

- (NSEnumerator *)keyEnumerator
{
    return [self.dictionary keyEnumerator];   
}

#pragma -
#pragma mark - internal
+ (NSString *)encodeToPercentEscapeString:(NSString *)string
{
    if ([string isKindOfClass:[NSString class]])
        return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)(string), NULL, (__bridge CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
    else
        return string;
}

- (NSString *) generateParamString {
    
    NSMutableArray * postParametrs = [NSMutableArray array];
    
    for ( NSString * key in dictionary_ ) {
        
        id  value = [dictionary_ valueForKey: key];
        
        if ( [[value description] length] > 0 )
        {
            value = [URLDictionary encodeToPercentEscapeString: value];
            [postParametrs addObject:  CreateKeyValuePair(key, value)];
        }
    }
    
    return [postParametrs componentsJoinedByString:@"&"];
}



- (NSData *) postParametrs {
    
    return [[self generateParamString] dataUsingEncoding: NSUTF8StringEncoding];
}

- (NSString *) getParametrs {
    
    return [self generateParamString];
}

@end
