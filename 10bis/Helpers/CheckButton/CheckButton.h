#import <Foundation/Foundation.h>
@interface CheckButton : UIButton 
{
    BOOL isChecked;
    UIImage *emptyImage_;
    UIImage *filledImage_;
}

// create button with images
+ (id)checkButtonWithEmptyImage:(UIImage*)emptyImage filledImage:(UIImage*)filledImage;

// reset current state to default
- (void)resetButton;

// current state
@property (nonatomic, unsafe_unretained) BOOL checked;
@property (nonatomic, strong) UIImage *emptyImage;
@property (nonatomic, strong) UIImage *filledImage;

@end
