#import "CheckButton.h"

@interface CheckButton ()
@end

@implementation CheckButton
@synthesize checked = isChecked;
@synthesize emptyImage = emptyImage_;
@synthesize filledImage = filledImage_;

#pragma -
#pragma INHERIT

+ (id)checkButtonWithEmptyImage:(UIImage*)emptyImage filledImage:(UIImage*)filledImage
{
    CheckButton *returnButton = [CheckButton buttonWithType: UIButtonTypeCustom];
    [returnButton setEmptyImage: emptyImage];
    [returnButton setFilledImage: filledImage];
    
    [returnButton setImage: emptyImage forState: UIControlStateNormal];
    [returnButton setImage: filledImage forState: UIControlStateSelected];
    return returnButton;
}
- (NSString*)description
{
    NSString *superDescription = [super description];
    NSString *uncheckedImage = [NSString stringWithFormat: @"Uncheked image: %@", emptyImage_];
    NSString *checkedImage = [NSString stringWithFormat: @"Checked image: %@", filledImage_];
    NSString *isSelected = [NSString stringWithFormat: @"Button checked: %@", isChecked ? @"YES":@"NO"];
    NSString *description = [NSString stringWithFormat: @"%@\n%@\n%@\n%@", superDescription, uncheckedImage, checkedImage, isSelected];
    return description;
}
#pragma -
#pragma Action

- (void)sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event
{
    isChecked = !isChecked;
   
    if (isChecked) 
    {   
        [self setImage: [self filledImage] forState: UIControlStateNormal];
    }
    else
    {
        [self setImage: [self emptyImage] forState: UIControlStateNormal];
    }

    [super sendAction:action to:target forEvent:event];
}

- (void)resetButton
{
    isChecked = NO;
    [self setImage: [self emptyImage] forState: UIControlStateNormal];
}
- (void)setChecked:(BOOL)checked
{
    isChecked = checked;
    if (isChecked) 
    {   
        [self setImage: [self filledImage] forState: UIControlStateNormal];
    }
    else
    {
        [self setImage: [self emptyImage] forState: UIControlStateNormal];
    }

}

- (void)setEmptyImage:(UIImage *)emptyImage
{
    emptyImage_ = emptyImage;
    [self setChecked: isChecked];
}
- (void)setFilledImage:(UIImage *)filledImage
{
    filledImage_ = filledImage;
    [self setChecked: isChecked];
}

@end
