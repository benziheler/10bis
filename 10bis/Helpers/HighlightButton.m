//
//  HighlightButton.m
//  10bis
//
//  Created by Anton Vilimets on 9/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "HighlightButton.h"

@implementation HighlightButton
{
    UIColor *defaultColor;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        defaultColor = self.backgroundColor;
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if(animated)
    {
        [UIView animateWithDuration:0.5 animations:^{
                    self.backgroundColor = highlighted ? self.tintColor : defaultColor;
        }];
    }
    else
    {
        self.backgroundColor = highlighted ? self.tintColor : defaultColor;
    }
}

- (void)setHighlighted:(BOOL)highlighted
{
    self.backgroundColor = highlighted ? self.tintColor : defaultColor;

}

@end
