//
//  NSDate+Helpers.m
//  PMCalendar
//
//  Created by Pavel Mazurin on 7/14/12.
//  Copyright (c) 2012 Pavel Mazurin. All rights reserved.
//

#import "NSDate+Helpers.h"

@implementation NSDate (Helpers)

- (NSDate *)dateWithoutTime
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components = [calendar components:(NSYearCalendarUnit 
                                                          | NSMonthCalendarUnit 
                                                          | NSDayCalendarUnit ) 
                                                fromDate:self];
	
	return [calendar dateFromComponents:components];
}

- (NSDate *)timeDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *components = [calendar components:(NSHourCalendarUnit
                                                         | NSMinuteCalendarUnit)
                                               fromDate:self];
    [components setMinute:components.minute+1];
	NSDate *result = [calendar dateFromComponents:components];
	return result;
}

- (NSDate *) dateByAddingDays:(NSInteger) days months:(NSInteger) months years:(NSInteger) years
{
	NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
	dateComponents.day = days;
	dateComponents.month = months;
	dateComponents.year = years;
	NSDate *result = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents
                                                                   toDate:self
                                                                  options:0];
    return result;
}

- (NSDate *) dateByAddingDays:(NSInteger) days
{
    return [self dateByAddingDays:days months:0 years:0];
}

- (NSDate *) dateByAddingMonths:(NSInteger) months
{
    return [self dateByAddingDays:0 months:months years:0];
}

- (NSDate *) dateByAddingYears:(NSInteger) years
{
    return [self dateByAddingDays:0 months:0 years:years];
}

- (NSDate *) monthStartDate 
{
    NSDate *monthStartDate = nil;
	[[NSCalendar currentCalendar] rangeOfUnit:NSMonthCalendarUnit
                                    startDate:&monthStartDate 
                                     interval:NULL
                                      forDate:self];

	return monthStartDate;
}

- (NSUInteger) numberOfDaysInMonth
{
    return [[NSCalendar currentCalendar] rangeOfUnit:NSDayCalendarUnit 
                                              inUnit:NSMonthCalendarUnit 
                                             forDate:self].length;
}

- (NSUInteger) weekday
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *weekdayComponents = [gregorian components:NSWeekdayCalendarUnit fromDate:self];
    return [weekdayComponents weekday];
}

- (NSString *) dateStringWithFormat:(NSString *) format
{
  //  NSLocale *hebrew = [[NSLocale alloc] initWithLocaleIdentifier:@"he_IL"]; // Hebrew, Israel
   // NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSHebrewCalendar];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //formatter.locale = hebrew;
    //formatter.calendar = calendar;
    [formatter setDateFormat:format];
	NSString *result = [formatter stringFromDate:self];
	return result;
}

- (NSInteger) daysSinceDate:(NSDate *) date
{
    return [self timeIntervalSinceDate:date] / (60 * 60 * 24);
}

+ (NSDate *)dateFromString:(NSString *)date format:(NSString *)format
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:format];
    NSDate *myDate = [df dateFromString: date];
    return myDate;
}

- (NSDate *)roundedDate:(BOOL)up
{
    NSDateComponents *time = [[NSCalendar currentCalendar]
                             components:NSHourCalendarUnit | NSMinuteCalendarUnit 
                             fromDate:self];
    NSInteger minutes = [time minute];
    NSInteger hours = [time hour];
    if(minutes < 30)
    {
        if(up)
        {
        [time setHour:hours-1];
        }
        else
        {
            [time setHour:hours+1];
        }
    }
    else if(!up)
    {
        [time setHour:hours+2];

    }
    [time setMinute: 0];
    NSDate *res = [[NSCalendar currentCalendar] dateFromComponents:time];
    return res;
}

- (NSInteger)hours
{
    NSDateComponents *time = [[NSCalendar currentCalendar]
                              components:NSHourCalendarUnit | NSMinuteCalendarUnit
                              fromDate:self];
    NSInteger hours = [time hour];
    return hours;
}

- (NSInteger)minutes
{
    NSDateComponents *time = [[NSCalendar currentCalendar]
                              components:NSHourCalendarUnit | NSMinuteCalendarUnit
                              fromDate:self];
    NSInteger min = [time minute];
    return min;
}


- (NSInteger)day
{
    NSDateComponents *time = [[NSCalendar currentCalendar]
                              components:NSDayCalendarUnit | NSMinuteCalendarUnit
                              fromDate:self];
    NSInteger day = [time day];
    return day;
}

- (NSInteger)month
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger month = [components month];
    return month;
}

+ (NSDate *)setHours:(NSInteger)hours toDate:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:date];
    
    components.hour = hours;
    NSDate *resDate = [calendar dateFromComponents:components];
    return resDate;
}

+ (NSDate *)setMinutes:(NSInteger)minutes toDate:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:date];
    
    components.minute = minutes;
    NSDate *resDate = [calendar dateFromComponents:components];
    return resDate;
}

+ (NSDate *)setSeconds:(NSInteger)seconds toDate:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:date];
    
    components.second = seconds;
    NSDate *resDate = [calendar dateFromComponents:components];
    return resDate;
}



@end
