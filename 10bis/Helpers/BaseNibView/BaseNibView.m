//
//  BaseNibView.m
//  FBTestApp
//
//  Created by Anton Vilimets on 8/27/13.
//  Copyright (c) 2013 Anton Vilimets. All rights reserved.
//

#import "BaseNibView.h"

@implementation BaseNibView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (id)viewFromNib
{
    NSString *name = NSStringFromClass(self.class);
    NSArray *xib = [[NSBundle mainBundle] loadNibNamed:name  owner: self options: nil];
    return xib[0];
}


@end
