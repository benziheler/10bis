//
//  Created by Almog Cohen on 12.3.12.
//  Copyright (c) 2012 Almog Cohen All rights reserved.
//

#import "ACCoreGraphicsBasics.h"


void drawStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, 
                CGColorRef color, CGFloat width) {
    CGContextSaveGState(context);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetLineWidth(context, width);
    if (width == 1.0) {
        CGContextSetLineCap(context, kCGLineCapSquare);
        CGContextMoveToPoint(context, startPoint.x + 0.5, startPoint.y + 0.5);
        CGContextAddLineToPoint(context, endPoint.x + 0.5, endPoint.y + 0.5);
    } else {
        CGContextMoveToPoint(context, startPoint.x, startPoint.y);
        CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
    }
    CGContextStrokePath(context);
    //    CGContextRestoreGState(context);        
}

void drawRect(CGContextRef context, CGRect rect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGFloat cornerRadius) {
    CGContextSaveGState(context);
    if (cornerRadius > 0) { 
        createRoundedCorners(context, cornerRadius, rect);
        CGContextClip(context);
    }
    
    //draw fill
    if (fillColor!=nil) {
        CGContextSetFillColorWithColor(context, fillColor);
        CGContextFillRect(context, rect);
    }
    
    //draw border
    if (borderColor!=nil) {
        CGContextSetStrokeColorWithColor(context, borderColor);
        CGContextSetLineWidth(context, borderWidth);
        CGContextStrokeRect(context, CGRectInset(rect, borderWidth/2, borderWidth/2));
    }
    
    CGContextRestoreGState(context);
}

void drawRoundedRect(CGContextRef context, CGRect rrect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGFloat cornerRadius) {
    
    CGContextSetStrokeColorWithColor(context, borderColor);
    CGContextSetFillColorWithColor(context, fillColor);
    if (borderWidth>1)
        CGContextSetLineWidth(context, borderWidth);
    
    CGFloat minx = CGRectGetMinX(rrect), midx = CGRectGetMidX(rrect), maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect), midy = CGRectGetMidY(rrect), maxy = CGRectGetMaxY(rrect);
    
    
    {
        // Start at 1
        CGContextMoveToPoint(context, minx, midy);
        // Add an arc through 2 to 3
        CGContextAddArcToPoint(context, minx, miny, midx, miny, cornerRadius);
        // Add an arc through 4 to 5
        CGContextAddArcToPoint(context, maxx, miny, maxx, midy, cornerRadius);
        // Add an arc through 6 to 7
        CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, cornerRadius);
        // Add an arc through 8 to 9
        CGContextAddArcToPoint(context, minx, maxy, minx, midy, cornerRadius);
        // Close the path
        CGContextClosePath(context);     
        
        // Fill & stroke the path
        CGContextDrawPath(context, kCGPathFillStroke);
    }
    
}

void drawRectWithShadow(CGContextRef context, CGRect rect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGFloat cornerRadius) {
    CGContextSaveGState(context);
    if (cornerRadius > 0) { 
        createRoundedCorners(context, cornerRadius, rect);
        CGContextClip(context);
    }
    CGContextSetShadow(context, CGSizeMake(0, 2), 3.0);
    //draw fill
    if (fillColor!=nil) {
        CGContextSetFillColorWithColor(context, fillColor);
        CGContextFillRect(context, rect);
    }
    
    //draw border
    if (borderColor!=nil) {
        CGContextSetStrokeColorWithColor(context, borderColor);
        CGContextSetLineWidth(context, borderWidth);
        CGContextStrokeRect(context, CGRectInset(rect, borderWidth/2, borderWidth/2));
    }
    
    CGContextRestoreGState(context);
}

void drawRectWithShadowCustom(CGContextRef context, CGRect rect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGColorRef shadowColor, CGSize shadowOffset, CGFloat shadowBlur, CGFloat cornerRadius) {
    CGContextSaveGState(context);
    if (cornerRadius > 0) { 
        createRoundedCorners(context, cornerRadius, rect);
        CGContextClip(context);
    }
    CGContextSetShadowWithColor(context, shadowOffset, shadowBlur, shadowColor);
    //draw fill
    if (fillColor!=nil) {
        CGContextSetFillColorWithColor(context, fillColor);
        CGContextFillRect(context, rect);
    }
    
    //draw border
    if (borderColor!=nil) {
        CGContextSetStrokeColorWithColor(context, borderColor);
        CGContextSetLineWidth(context, borderWidth);
        CGContextStrokeRect(context, CGRectInset(rect, borderWidth/2, borderWidth/2));
    }
    
    CGContextRestoreGState(context);
}

void drawGloss(CGContextRef context, CGRect rect) {
    
    CGRect topHalf = CGRectMake(rect.origin.x, rect.origin.y, 
                                rect.size.width, rect.size.height/2);
    drawGradientOfTwoColors(context, topHalf, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.35].CGColor, [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.1].CGColor, 0);
}

void drawGradientOfTwoColors(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef  endColor, CGFloat cornerRadius) {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)startColor, (__bridge id)endColor, nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    if (cornerRadius > 0) { 
        createRoundedCorners(context, cornerRadius, rect);
        CGContextClip(context);
    }
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

void drawGradientOfThreeColors(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef midColor, CGColorRef  endColor, CGFloat cornerRadius) {
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0,0.5, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)startColor, (__bridge id)midColor, (__bridge id)endColor, nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    if (cornerRadius > 0) { 
        createRoundedCorners(context, cornerRadius, rect);
        CGContextClip(context);
    }
    CGContextAddRect(context, rect);
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

void createRoundedCorners(CGContextRef context, CGFloat radius, CGRect _rect) {
    float fw, fh;
    float ovalWidth, ovalHeight;
    
    if (radius == 0) {
        CGContextAddRect(context, _rect);
        return;
    }
    
    ovalWidth = ovalHeight = radius;
    
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(_rect), CGRectGetMinY(_rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (_rect) / ovalWidth;
    fh = CGRectGetHeight (_rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    
    //Should be:
    //    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    //    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    
    //But I changed it to this:
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, (ovalHeight > _rect.size.height/2?0:1));
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, (ovalHeight > _rect.size.height/2?0:1));
    //So when the radius of the corners is larger than the logical size, we cancel the bottom corners
    
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

CGRect rectFor1PxStroke(CGRect rect) {
    return CGRectMake(rect.origin.x + 0.5, rect.origin.y + 0.5, 
                      rect.size.width - 1, rect.size.height - 1);
}