#import <UIKit/UIKit.h>

typedef void(^DownloadDataCompletionBlock)(NSData *result, NSError *error);
typedef void(^DownloadImageCompletionBlock)(UIImage *result, NSError *error);
typedef void(^DownloadStringCompletionBlock)(NSString *result, NSError *error);

@interface Downloader : NSObject

+ (void)dataDownloadWithRequest:(NSURLRequest*)request completion:(DownloadDataCompletionBlock)completion;
+ (void)imageDownloadWithRequest:(NSURLRequest*)request completion:(DownloadImageCompletionBlock)completion;
+ (void)stringDownloadWithRequest:(NSURLRequest*)request completion:(DownloadStringCompletionBlock)completion;

@end

@interface NSString (BKDownload)

- (NSURLRequest*)URLRequest;

@end
