//
//  NSObject+JSONDictionary.m
//  10bis
//
//  Created by Vadim Pavlov on 12.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "NSObject+JSONDictionary.h"
#import <objc/runtime.h>

@implementation NSObject (JSONDictionary)
- (BOOL)isSimpleClass:(id)object
{
    if ([object isKindOfClass: [NSString class]] ||
        [object isKindOfClass: [NSNumber class]])
    {
        return YES;
    }
    return NO;
}
- (NSDictionary*)jsonDictionary
{
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    
    NSMutableDictionary *json = [NSMutableDictionary dictionaryWithCapacity: outCount];
    
    for(i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        
        NSString *propNameStr = [NSString stringWithCString: propName encoding: NSUTF8StringEncoding];
        
        id value = [self valueForKey: propNameStr];
        if ([propNameStr isEqualToString: @"price"] && [value isEqualToNumber: @(0)])
        {
            // wrap 0 price in string
            value = @"0";
        }
        // call recursive function
        [self wrapValue: value inContainer: json forKey: propNameStr];
        
    }
    free(properties);
    
    return json;
}

- (void)wrapValue:(id)value inContainer:(id)container forKey:(NSString*)key
{
    if (value == nil)
        return;
    else if ([self isSimpleClass: value])
    {
        if ([value isKindOfClass: [NSNumber class]] && [value isEqualToNumber: @0])
        {
            return;
        }
        [self addValue: value toContainer: container forKey: key];

    }
    else if ([value isKindOfClass: [NSArray class]])
    {
        NSMutableArray *jsonArray = [NSMutableArray arrayWithCapacity: [value count]];
        for (id item in value)
        {
            [self wrapValue: item inContainer: jsonArray forKey: nil];
        }
        if (jsonArray.count)
            [self addValue: jsonArray toContainer: container forKey: key];
    }
    else if ([value isKindOfClass: [NSDictionary class]])
    {
       /* for (id key in value) {
            
        }*/
    }
    else
    {
        NSDictionary *json = [value jsonDictionary];
        [self addValue: json toContainer: container forKey: key];
    }
}
- (void)addValue:(id)value toContainer:(id)container forKey:(NSString*)key
{
    if ([container isKindOfClass: [NSDictionary class]])
    {
        NSMutableDictionary *dictinoary = container;
        [dictinoary setObject: value forKey: key];
    }
    else if ([container isKindOfClass: [NSArray class]])
    {
        NSMutableArray *array = container;
        [array addObject: value];
    }
}

@end
