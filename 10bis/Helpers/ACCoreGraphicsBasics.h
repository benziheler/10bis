//
//  Created by Almog Cohen on 12.3.12.
//  Copyright (c) 2012 Almog Cohen All rights reserved.
//

#import <Foundation/Foundation.h>

void drawStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, 
                CGColorRef color, CGFloat width);

void drawRect(CGContextRef context, CGRect rect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGFloat cornerRadius);

void drawRoundedRect(CGContextRef context, CGRect rrect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGFloat cornerRadius);

void drawRectWithShadow(CGContextRef context, CGRect rect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGFloat cornerRadius);

void drawRectWithShadowCustom(CGContextRef context, CGRect rect, CGColorRef fillColor, CGColorRef borderColor, CGFloat borderWidth, CGColorRef shadowColor, CGSize shadowOffset, CGFloat shadowBlur, CGFloat cornerRadius);

void drawGloss(CGContextRef context, CGRect rect);

void drawGradientOfTwoColors(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef  endColor, CGFloat cornerRadius);

void drawGradientOfThreeColors(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef midColor, CGColorRef  endColor, CGFloat cornerRadius);

void createRoundedCorners(CGContextRef context, CGFloat radius, CGRect _rect);

CGRect rectFor1PxStroke(CGRect rect);