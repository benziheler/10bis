//
//  NSArray+Sort.h
//  BlueBox
//
//  Created by OnO Apps on 06.11.12.
//  Copyright (c) 2012 Vadim Pavlov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Sort)
- (NSArray*) sortByObjectTag;
- (NSArray*) sortByUIViewOriginX;
- (NSArray*) sortByUIViewOriginY;
@end
