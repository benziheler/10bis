//
//  UIViewController+IOS7.m
//  10bis
//
//  Created by Vadim Pavlov on 24.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "UIViewController+IOS7.h"

@implementation UIViewController (IOS7)
- (UIRectEdge)edgesForExtendedLayout
{
    return UIRectEdgeNone;
}
@end
