//
//  AppDelegate.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "AppDelegate.h"
#import "DataManager.h"
#import <Crashlytics/Crashlytics.h>
#import <GigyaSDK/Gigya.h>
#import "SlidingVC.h"
#import "AppsFlyerTracker.h"
#import "TableFor2VC.h"


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"9GqVyp7E9oHwtSpuwWndPS";
    [AppsFlyerTracker sharedTracker].appleAppID = @"434368191";

    [GAI sharedInstance].dispatchInterval = 20;
    // Initialize tracker.
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-235558-15"];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"app_event"     // Event category (required)
                                                          action:@"launch"        // Event action (required)
                                                           label:@"application launched"          // Event label
                                                           value:nil] build]];
    
    if ([Utils isIOS7] == NO)
    {
        [application setStatusBarHidden: NO withAnimation:UIStatusBarAnimationSlide];
    }

    [Crashlytics startWithAPIKey:@"f0068115da4c06bf11b509483e559ac09d4a5e25"];
    [application setStatusBarStyle: UIStatusBarStyleLightContent];
    [DataManager class];
    
    NSString *languageCode = @"he";
    [[NSUserDefaults standardUserDefaults] setObject:@[languageCode] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     
    UIColor *tintColor = [Utils appRedColor];
    
    if ([Utils isIOS7])
    {
        [[UINavigationBar appearance] setTintColor: [UIColor whiteColor]];
        [[UINavigationBar appearance] setBarTintColor: tintColor];
    }
    else
    {
        [[UINavigationBar appearance] setTintColor: tintColor];
    }
    // setting empty image will broke Gigya's screens, check SlidingVC for customizing app navigation bar
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setBackgroundColor: [Utils appRedColor]];

    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName: @"Arial-BoldMT" size: 31]}];
    
    [DataManager loadCuisinesFilterList:^(id result, NSString *error) {
        if(result && !error)
        {
            [[NSUserDefaults standardUserDefaults] setObject:result forKey:kCuisinesFilterList];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }];
    
    return YES;
}

- (CustomWindow *)window
{
    static CustomWindow *customWindow = nil;
    if (!customWindow) customWindow = [[CustomWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    return customWindow;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"enterBgDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSDate *enterBgDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"enterBgDate"];
    if(enterBgDate)
    {
       
        NSTimeInterval distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:enterBgDate];
        NSInteger minutesBetweenDates = distanceBetweenDates / 60;
        if(minutesBetweenDates >= 40)
        {
            SlidingVC *rootVC = (SlidingVC *)self.window.rootViewController;
            [rootVC resetTopView];
            UINavigationController *topNavVC = (UINavigationController *)rootVC.topViewController;
            [topNavVC popToRootViewControllerAnimated:NO];
            //go to main
        }
    }
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[DataManager sharedManager] updateCurrentLocation];
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [Gigya handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    id presentedViewController = [window.rootViewController presentedViewController];
    
    
    NSString *className = presentedViewController ? NSStringFromClass([presentedViewController class]) : nil;

    
    if (window && ([className isEqualToString:@"MPInlineVideoFullscreenViewController"] || window != self.window)) {
        return UIInterfaceOrientationMaskAll;
    } else {
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (void)application:(UIApplication *)application willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation duration:(NSTimeInterval)duration
{
    if(newStatusBarOrientation == UIInterfaceOrientationPortrait)
    {
        SlidingVC *sliding = (SlidingVC *)self.window.rootViewController;
        UINavigationController *navVC =  (UINavigationController *)sliding.topViewController;
        CGRect frame =  navVC.navigationBar.frame;
        frame.origin.y = 20;
        navVC.navigationBar.frame = frame;
    }
}

@end
