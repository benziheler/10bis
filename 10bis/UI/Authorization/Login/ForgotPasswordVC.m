//
//  ForgotPasswordVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "DataManager.h"
@interface ForgotPasswordVC ()

@end

@implementation ForgotPasswordVC
{
    __weak IBOutlet UITextField *_emailTF;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (_isNewCustomer)
    {
        self.title = @"לקוח חדש";
    }
    else
    {
        self.title = @"לקוח רשום";
    }

}
- (IBAction)retrieve:(id)sender
{
    NSString *email = _emailTF.text;
    if ([Utils isEmailValid: email] == NO)
    {
        [self showValidationAlertFor: _emailTF];
        return;
    }
    [self showLoadingOverlay];
    __weak ForgotPasswordVC *weakSelf = self;
    [DataManager resetPassword: email completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        if (error)
            [Utils showErrorMessage: error];
        else
        {
            [weakSelf.navigationController popViewControllerAnimated: YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message: @"פנייתך התקבלה, הודעת מייל עם סיסמה חדשה תשלח אלייך בהקדם" delegate: nil cancelButtonTitle: @"אישור" otherButtonTitles: nil];
            [alert show];
        }
    }];
}

- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    textField.filled = NO;
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    textField.filled = textField.text.length > 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
