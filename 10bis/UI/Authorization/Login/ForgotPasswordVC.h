//
//  ForgotPasswordVC.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@interface ForgotPasswordVC : BaseVC
@property (nonatomic, assign) BOOL isNewCustomer;
@end
