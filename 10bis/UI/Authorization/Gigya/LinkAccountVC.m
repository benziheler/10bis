//
//  LinkAccountVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "LinkAccountVC.h"
#import "DataManager.h"
#import <GigyaSDK/GSUser.h>
#import "ForgotPasswordVC.h"
#import "CartVC.h"
@interface LinkAccountVC () <UITextFieldDelegate>

@end

@implementation LinkAccountVC
{
    __weak IBOutlet AppTextField *_emailTF;
    __weak IBOutlet AppTextField *_passwordTF;

    __weak IBOutlet UIView *_socialHeader;
    __weak IBOutlet UIImageView *_socialIcon;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (_isNewCustomer)
    {
        self.title = @"לקוח חדש";
    }
    else
    {
        self.title = @"לקוח רשום";
    }
    
    _socialHeader.backgroundColor = [Utils colorForSocialNetwork: _socialNetwork];
    _socialIcon.image = [Utils iconForSocialNetwork: _socialNetwork];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"ForgotPasswordSegue"])
    {
        ForgotPasswordVC *vc = segue.destinationViewController;
        vc.isNewCustomer = _isNewCustomer;
    }
}
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated: YES];
}
- (IBAction)enter:(id)sender
{
    NSString *email = _emailTF.text;
    NSString *password = _passwordTF.text;
    if ([Utils isEmailValid: email] == NO)
    {
        [self showValidationAlertFor: _emailTF];
        return;
    }
    else if (password.length == 0)
    {
        [self showValidationAlertFor: _passwordTF];
        return;
    }
    [self showLoadingOverlay];
    __weak LinkAccountVC *weakSelf = self;
    [DataManager loginUser: email withPassword: password socialUID: _socialUser.UID encryptedId:nil completion:^(id result, NSString *error) {
        [self removeLoadingOverlay];
        if (error)
            [Utils showErrorMessage: error];
        else
        {
            UIViewController *rootVC = weakSelf.navigationController.viewControllers[0];
            DeliveryMethod method = [DataManager deliveryMethod];
            if (method == DeliveryMethodNone)
            {
                [weakSelf.navigationController popToRootViewControllerAnimated: YES];
            }
            else
            {
                if ([DataManager isLoginForCheckout])
                {
                    for (UIViewController *vc in weakSelf.navigationController.viewControllers)
                    {
                        if ([vc isKindOfClass: [CartVC class]])
                        {
                            [weakSelf.navigationController popToViewController: vc animated: NO];
                            [(CartVC*)vc continueOrder: nil];
                            return;
                        }
                    }
                }

                [weakSelf.navigationController popToRootViewControllerAnimated: NO];
                [rootVC performSegueWithIdentifier: @"addressSegue" sender: weakSelf];
            }
        }
    }];
}

#pragma mark - Text Fields
- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    textField.filled = NO;
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    textField.filled = textField.text.length > 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _emailTF)
    {
        [_passwordTF becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}
@end
