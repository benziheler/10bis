//
//  CreateAccountVC.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@class GSUser;
@interface CreateAccountVC : BaseVC
@property (nonatomic, strong) GSUser *socialUser;
@property (nonatomic, strong) NSString *socialNetwork;
@property (nonatomic, assign) BOOL isNewCustomer;

@end
