//
//  TransactionActionCell.h
//  10bis
//
//  Created by Anton Vilimets on 9/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionActionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;

- (void)setupWithDict:(NSDictionary *)dict;

@end
