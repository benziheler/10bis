//
//  TransactionReportVC.m
//  10bis
//
//  Created by Anton Vilimets on 9/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "TransactionReportVC.h"
#import "NSDate+Helpers.h"
#import "TransactionCell.h"
#import "TransactionActionCell.h"
#import "UIControl+BBlock.h"
#import "CustomAlert.h"
#import "DataManager.h"
#import "ReviewView.h"
#import "URLDictionary.h"

@interface TransactionReportVC ()
{
    NSInteger _currentPage;
}

@property (strong, nonatomic) ReviewView *reviewView;

@end

@implementation TransactionReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.translatesAutoresizingMaskIntoConstraints = YES;
    self.reviewView = [ReviewView viewFromNib];
   // id <UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    _reviewView.frame = [UIScreen mainScreen].bounds;

    _nextButton.hidden = YES;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.backgroundView = nil;
    _tableView.shouldExpandOnlyOneCell = YES;
    _tableView.SKSTableViewDelegate = self;
    self.title = @"דו\"ח חיובים";
    _prevButton.layer.cornerRadius = _nextButton.layer.cornerRadius = 3;
    [_tableView registerNib:[UINib nibWithNibName:@"TransactionCell" bundle:nil] forCellReuseIdentifier:@"TransactionCell"];
    [_tableView registerNib:[UINib nibWithNibName:@"TransactionActionCell" bundle:nil] forCellReuseIdentifier:@"TransactionActionCell"];

    [self updateDatasourceWithCurrMonth];
    __weak TransactionReportVC *weakSelf = self;
    [_reviewView setSendReviewCompletion:^(NSInteger reviewId, NSNumber *transactionId, NSString *comment, NSNumber *rate) {
         for (int j=0;j<weakSelf.currentDatasource.count; j++) {
             NSArray *section = weakSelf.currentDatasource[j];
            for (int i=0;i<section.count; i++) {
                NSDictionary *dict = section[i];
                if([dict[@"TransactionId"] isEqualToNumber:transactionId])
                {
                    NSMutableDictionary *modifiedDict = [dict mutableCopy];
                    modifiedDict[@"ReviewId"] = @(reviewId);
                    modifiedDict[@"ReviewDate"] = [NSString stringWithFormat:@"/Date(%.f)/", [[NSDate date] timeIntervalSince1970]];
                    modifiedDict[@"ReviewText"] = comment ? comment : @"";
                    modifiedDict[@"ReviewRank"] = rate;
                    NSMutableArray *modifiedArr =[section mutableCopy];
                    modifiedArr[i] = modifiedDict;
                    weakSelf.currentDatasource[[weakSelf.currentDatasource indexOfObject:section]] = modifiedArr;
                    
                        [weakSelf.tableView reloadData];

                    break;
                }
            }
        }
    }];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _reviewView.frame = [UIScreen mainScreen].bounds;
}

- (void)updateDatasourceWithCurrMonth
{
    [self updateDateLabel];
    _noResLabel.hidden = YES;
    [self showLoadingOverlay];
    if(_currentDatasource.count)
    {
        _tableView.expandableCells = nil;
        _currentDatasource = nil;
        [_tableView reloadData];
    }
    _prevButton.userInteractionEnabled = NO;
    _nextButton.userInteractionEnabled = NO;
    __weak TransactionReportVC *weakSelf = self;
    [DataManager loadTransactionsOfUserForMonth:_currentPage completion:^(id result, NSString *error) {
        [weakSelf buildDatasourceFromObjects:result];
        weakSelf.noResLabel.hidden = weakSelf.currentDatasource.count;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.tableView reloadData];
            [weakSelf.tableView setContentOffset:CGPointZero animated:NO];
            [weakSelf removeLoadingOverlay];
            _prevButton.userInteractionEnabled = YES;
            _nextButton.userInteractionEnabled = YES;
        });
    }];
}

- (void)buildDatasourceFromObjects:(NSArray *)objects
{
    NSArray *sortedArray = [[objects valueForKey:@"CardSuffix"] sortedArrayUsingComparator:^(id a, id b) {
        return [a compare:b options:NSNumericSearch];
    }];

    NSOrderedSet *set = [NSOrderedSet orderedSetWithArray:sortedArray];
    
    NSMutableArray *datasource = [NSMutableArray array];

    for (NSString *suffix in set) {
         NSArray *filtered = [objects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CardSuffix == %@)", suffix]];
        [datasource addObject:filtered];
    }
    self.currentDatasource = datasource;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _currentDatasource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *arr = _currentDatasource[section];
    return arr.count;
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TransactionCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"TransactionCell"];
    NSArray *arr = _currentDatasource[indexPath.section];
    [cell setupWithDict:arr[indexPath.row]];
    cell.expandable = [self canExpandObj:arr[indexPath.row]];
    return cell;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    TransactionActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionActionCell" forIndexPath:indexPath];
    NSArray *arr = _currentDatasource[indexPath.section];
    [cell setupWithDict:arr[indexPath.row]];
    [cell.orderButton addActionForControlEvents:UIControlEventTouchUpInside withBlock:^(id control, UIEvent *event) {
        [self orderWithObj:arr[indexPath.row]];
    }];
    
    [cell.reviewButton addActionForControlEvents:UIControlEventTouchUpInside withBlock:^(id control, UIEvent *event) {
        [self reviewWithObj:arr[indexPath.row]];
    }];
    return cell;
}

- (void)collapseSubrows
{
    [self.tableView collapseCurrentlyExpandedIndexPaths];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, 30)];
    view.backgroundColor = RGB(199, 36, 34);
    UILabel *lbl = [[UILabel alloc] initWithFrame:view.bounds];
    [view addSubview:lbl];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor whiteColor];
    NSDictionary *obj = [self.currentDatasource[section] firstObject];
    lbl.text = [self headerTitleForObject:obj];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *arr = _currentDatasource[indexPath.section];
    NSDictionary *dict = arr[indexPath.row];
    NSString *name = dict[@"ResName"];
    CGFloat textHeight = MAX([self heightForText:name] + 10, 52);
    return textHeight;
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSString *)headerTitleForObject:(NSDictionary *)obj
{
    if([obj[@"PaymentMethod"] isEqualToString:@"Moneycard"])
    {
        return [NSString stringWithFormat:@"XXX-%@ %@", obj[@"CardSuffix"], @"חיובים בכרטיס תן ביס"];
    }
    else if ([obj[@"PaymentMethod"] isEqualToString:@"Creditcard"])
    {
        return [NSString stringWithFormat:@"XXXX-%@ %@", obj[@"CardSuffix"], @"חיובים בכרטיס אשראי "];
    }
    else if ([obj[@"PaymentMethod"] isEqualToString:@"Cash"])
    {
        return @"חיובים במזומן";
    }
    return obj[@"CardSuffix"];
}

-(void)reviewWithObj:(NSDictionary *)obj
{
    if([obj[@"ReviewId"] integerValue] == 0)
    {
        [_reviewView sendReviewWithObj:obj];
    }
    else
    {
        [_reviewView showReviewWithObj:obj];
    }
}

-(void)orderWithObj:(NSDictionary *)obj
{
    NSString *urlString = [NSString stringWithFormat:@"https://www.10bis.co.il/Account/GetSingleOrderViewInfo?orderId=%@&%@&currentContext=MobileApp&shoppingCartGuid=%@",obj[@"OrderID"], [[DataManager baseParams] getParametrs], [DataManager shoppingCart]];
    CustomAlert *alert = [[CustomAlert alloc] initWithTitle: nil msg: nil button: nil alignment: NSTextAlignmentCenter link: urlString];
    alert.needToResize = YES;
    CGRect frame = self.view.window.bounds;
    frame = CGRectOffset(frame, 20, 30);
    frame.size.width -= 40;
    frame.size.height -= 50;
    alert.whiteBackgroundView.frame = frame;
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, frame.size.width - 100, 60)];
    lbl.textColor = [Utils appRedColor];
    lbl.text = @"הזמנה";
    lbl.font = [UIFont boldSystemFontOfSize:20];
    lbl.textAlignment = NSTextAlignmentCenter;
    [alert.whiteBackgroundView addSubview:lbl];

    [alert.webView setScalesPageToFit:NO];
    alert.webView.frame = alert.whiteBackgroundView.bounds;
    alert.webView.y = 60;
    alert.webView.height -= 60;
    alert.webView.x -= 2;
    alert.whiteBackgroundView.clipsToBounds = YES;
    [alert showAlertAnimated:YES];
}

- (IBAction)prevPressed:(id)sender
{
    _currentPage--;
    [self updateDatasourceWithCurrMonth];
}

- (IBAction)nextPressed:(id)sender
{
    _currentPage++;
    [self updateDatasourceWithCurrMonth];
}

- (void)updateDateLabel
{
    _nextButton.hidden = (_currentPage >= 0);
    _nextLarge.hidden = _nextButton.hidden;
    NSDate *date = [[NSDate date] dateByAddingMonths:_currentPage];
    
    NSDate *prevDate = [[NSDate date] dateByAddingMonths:_currentPage-1];
    NSDate *nextDate = [[NSDate date] dateByAddingMonths:_currentPage+1];
    NSString *prevStr = [Utils hebrewMonthString:[prevDate month] isShort:YES];

    NSString *nextStr = [Utils hebrewMonthString:[nextDate month] isShort:YES];
    [_nextButton setTitle:nextStr forState:UIControlStateNormal];
    [_prevButton setTitle:prevStr forState:UIControlStateNormal];

   
    NSString *str = [NSString stringWithFormat:@"%@ %@", [Utils hebrewMonthString:[date month] isShort:NO], [date dateStringWithFormat:@"YYYY"]];
    CATransition *animation = [CATransition animation];
    animation.duration = 0.6f;
    animation.type = kCATransitionFade;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [_dateLabel.layer addAnimation:animation forKey:@"changeTextTransition"];
    _dateLabel.text = str;
}

- (CGFloat)heightForText:(NSString*)headerText
{
    NSMutableParagraphStyle *hebrewParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    [hebrewParagraphStyle setAlignment:NSTextAlignmentRight];
    
    NSMutableAttributedString *descr = [[NSMutableAttributedString alloc] initWithString:headerText attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16], NSParagraphStyleAttributeName: hebrewParagraphStyle}];
    if([[UIDevice currentDevice] systemVersion].floatValue >= 7.0f)
    {
        [descr addAttribute:NSWritingDirectionAttributeName value:@[@(NSWritingDirectionRightToLeft | NSTextWritingDirectionOverride)] range:NSMakeRange(0, headerText.length)];
    }
    
     CGSize size = [descr boundingRectWithSize: CGSizeMake(140, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
    return size.height;
}

- (BOOL)canExpandObj:(NSDictionary *)obj
{
    if([obj[@"TransactionType"] isEqualToString:@"terminal"] == NO || [obj[@"ReviewId"] integerValue] != 0 || [obj[@"AddReviewDisabled"] boolValue] == NO)
    {
        return YES;
    }
    return NO;
}

@end
