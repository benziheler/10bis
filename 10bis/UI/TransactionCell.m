//
//  TransactionCell.m
//  10bis
//
//  Created by Anton Vilimets on 9/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "TransactionCell.h"
#import "NSDate+Helpers.h"

@implementation TransactionCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setupWithDict:(NSDictionary *)dict
{
    _nameLabel.text = dict[@"ResName"];
    _amountLabel.text =  [NSString stringWithFormat:@"₪ %.2f",[dict[@"TransactionAmount"] floatValue]];
    NSString *timestamp = [dict[@"TransactionDate"] stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
    timestamp = [timestamp stringByReplacingOccurrencesOfString:@")/" withString:@""];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp.doubleValue/1000];
    _dateLabel.text = [date dateStringWithFormat:@"hh:mm dd/M/YYYY"];
    
    if([dict[@"TransactionType"] isEqualToString:@"websiteorder"])
    {
        self.icnImageView.image = [UIImage imageNamed:@"icn_delivery"];
    }
    else if([dict[@"TransactionType"] isEqualToString:@"terminal"])
    {
        self.icnImageView.image = [UIImage imageNamed:@"icn_card"];
    }
    else if([dict[@"TransactionType"] isEqualToString:@"pickup"])
    {
        self.icnImageView.image = [UIImage imageNamed:@"icn_pickup"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
