//
//  RestaurantCell.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Restaurant;
@interface RestaurantCell : UITableViewCell
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, strong) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImgView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UIView *pickupDetailView;
@property (weak, nonatomic) IBOutlet UIView *deliveryDetailView;
@property (weak, nonatomic) IBOutlet UILabel *sumDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeDeliveryLabel;
@end
