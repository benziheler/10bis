//
//  RestaurantCell.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantCell.h"
#import "Restaurant.h"
#import "DataManager.h"
#import "CustomAlert.h"
#import "StarsView.h"

@implementation RestaurantCell
{
    __weak IBOutlet UILabel *_nameLabel;
    __weak IBOutlet UILabel *_addressLabel;
    
    __weak IBOutlet UIImageView *distanceImgView;
    __weak IBOutlet UILabel *_phoneLabel;
    __weak IBOutlet UILabel *_workHoursLabel;
    __weak IBOutlet UILabel *_reviewsLabel;
    __weak IBOutlet UIImageView *rightIcon;

    __weak IBOutlet UILabel *deliveryPhoneLabel;
    __weak IBOutlet UIImageView *leftIcon;
    __weak IBOutlet UIImageView *_logoImageView;
    __weak IBOutlet UILabel *_discountLabel;
    __weak IBOutlet UIButton *_discountRibbonButton;
    __weak IBOutlet StarsView *_starsView;
    __weak IBOutlet UIButton *_categoryButton;
    __weak IBOutlet UIImageView *_kosherIcon;
    CGRect _initialCouponRect;
    CGRect _initialCusineRect;
}
- (void)awakeFromNib
{
    

    _discountLabel.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-30));
    _logoImageView.layer.cornerRadius = 2.0f;
    _logoImageView.layer.masksToBounds = YES;
    _initialCusineRect = _categoryButton.frame;

    UIImage *bgImage = [_categoryButton backgroundImageForState: UIControlStateNormal];
    UIImage *strBgImage = [bgImage stretchableImageWithLeftCapWidth: bgImage.size.width/2 topCapHeight:bgImage.size.height/2];
    [_categoryButton setBackgroundImage: strBgImage forState: UIControlStateNormal];
}
- (void)setRestaurant:(Restaurant *)restaurant
{
    _starsView.isRTL = YES;
    CGRect kosherFrame = _kosherIcon.frame;
    kosherFrame.origin.y = _categoryButton.frame.origin.y;
    _kosherIcon.frame = kosherFrame;
    _bgImageView.hidden = NO;
    NSString *oldLogoURL = _restaurant.logoURL;
    _restaurant = restaurant;
    _nameLabel.text = restaurant.name;
    leftIcon.hidden = NO;
    rightIcon.hidden = NO;
    leftIcon.image = [UIImage imageNamed:@"icn_phone"];
        rightIcon.image = [UIImage imageNamed:@"icn_hours"];

    //orr
    _kosherIcon.hidden = ![restaurant.isKosher isEqualToString: @"Kosher"];
    
    NSMutableArray *contactArray = [NSMutableArray arrayWithCapacity: 2];

    if (_restaurant.street)
        [contactArray addObject: _restaurant.street];
    _addressLabel.text = [contactArray componentsJoinedByString: @", "];
    CLLocationCoordinate2D curLocation = [[DataManager sharedManager] currentLocation].coordinate;
    CLLocationDistance distance = 0.0f;
    if (curLocation.longitude != 0 && curLocation.longitude != 0)
    {
        CLLocation *restaurantLocation = [[CLLocation alloc] initWithLatitude: _restaurant.latitude longitude: _restaurant.longitude];
        CLLocation *userLocation = [[CLLocation alloc] initWithLatitude: curLocation.latitude longitude: curLocation.longitude];
        
        distance = [userLocation distanceFromLocation: restaurantLocation] / 1000;
    }
    _distanceLabel.text = [NSString stringWithFormat: @"      %0.2f %@", distance, @"ק״מ ממך"];
    
    // logo
    _logoImageView.image = [UIImage imageNamed: @"place_holder_70"];
    [Utils loadImage: restaurant.logoURL completion:^(UIImage *image) {
        if (_restaurant == restaurant && image)
        {
            _logoImageView.image = image;
            if ([oldLogoURL isEqualToString: restaurant.logoURL] == NO)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                 
                        _logoImageView.image = image;
                });
            }
        }
    }];

    [_starsView setStars: _restaurant.reviewsRank];
    _phoneLabel.text = _restaurant.phone;
    deliveryPhoneLabel.text = _restaurant.phone;

    
    BOOL couponHidden = NO;
    NSString *couponText = nil;
    DeliveryMethod method = [DataManager deliveryMethod];
    switch (method) {
        case DeliveryMethodDelivery:
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
            break;
        case DeliveryMethodPickup:
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
            break;
        case DeliveryMethodSitting:
            if (restaurant.isHappyHourActive == NO ||
                [restaurant.happyHourDiscount isEqualToString: @"0%"])
            {
                couponHidden = YES;
            }
            couponText = restaurant.happyHourDiscount;
            break;
        default:
            break;
    }
    _workHoursLabel.hidden = NO;
    _phoneLabel.hidden = NO;


    // discount
    _discountLabel.hidden = _discountRibbonButton.hidden = couponHidden;
    if ((method == DeliveryMethodSitting || restaurant.couponHasRestrictions) && couponText)
        couponText = [@"*" stringByAppendingString: couponText];
    
    _discountLabel.text = couponText;
    distanceImgView.hidden = NO;
    
    _deliveryDetailView.hidden = YES;
    _pickupDetailView.hidden = NO;
    
    // hours or delivery fee
    if (method == DeliveryMethodDelivery)
    {
        _deliveryDetailView.hidden = NO;
        _pickupDetailView.hidden = YES;
              NSString *minOrder = nil;
        NSString *deliveryPrice = nil;
        _workHoursLabel.hidden = NO;
        _phoneLabel.hidden = YES;
        _workHoursLabel.text = _phoneLabel.text;

        if (restaurant.minimumPriceForOrder == 0)
        {
            minOrder = @"אין מינימום להזמנה";
        }
        else
        {
            minOrder = [NSString stringWithFormat: @"מינימום: %ld ₪" , (long)restaurant.minimumPriceForOrder];
        }
        
        if (restaurant.deliveryPriceForOrder == 0)
        {
            deliveryPrice = @"משלוח חינם";
        }
        else
        {
            deliveryPrice = [NSString stringWithFormat: @"דמי משלוח: %ld ₪", (long)restaurant.deliveryPriceForOrder];
        }
        NSString *orderString = [NSString stringWithFormat: @"%@ | %@", _restaurant.poolSum, minOrder];
        if(_restaurant.isOverPoolMin)
        {
            orderString = @"ההזמנה עומדת בדרישת המינימום";
        }
        NSMutableAttributedString *attrOrder = [[NSMutableAttributedString alloc] initWithString: orderString];
        
        NSString *timeString = [NSString stringWithFormat: @"%@ | הזמנה עד %@", deliveryPrice, _restaurant.deliveryEndTime];
        NSMutableAttributedString *attrTime = [[NSMutableAttributedString alloc] initWithString: timeString];
        
        [Utils addBoldAttributeToString: attrOrder forText: orderString font: _workHoursLabel.font.pointSize];
        [Utils addBoldAttributeToString: attrTime forText: timeString font: _workHoursLabel.font.pointSize];
        _sumDeliveryLabel.attributedText = attrOrder;
        _timeDeliveryLabel.attributedText = attrTime;
        if(_restaurant.companyFlag == NO)
        {
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ | %@", minOrder, deliveryPrice]];
            _sumDeliveryLabel.attributedText = str;
            _timeDeliveryLabel.attributedText = nil;
            _timeDeliveryLabel.text = nil;
        }
    }
    else if(method == DeliveryMethodPickup)
    {
        NSString *hoursText = [NSString stringWithFormat: @"%@", _restaurant.pickupActivityHours];
        NSMutableAttributedString *attrHoursText = [[NSMutableAttributedString alloc] initWithString: hoursText];
        [Utils addBoldAttributeToString: attrHoursText forText: _restaurant.pickupActivityHours font: _workHoursLabel.font.pointSize];
        _workHoursLabel.attributedText = attrHoursText;
    }
    else if (method == DeliveryMethodSitting)
    {
        leftIcon.hidden = YES;
        rightIcon.image = [UIImage imageNamed:@"icn_phone"];
        _workHoursLabel.text = _phoneLabel.text;
        _phoneLabel.hidden = YES;
    }
    
    // reviews
    NSString *reviewsText = [NSString stringWithFormat: @"%ld המלצות", (long)_restaurant.numberOfReviews];
    NSMutableAttributedString *attrReviewsText = [[NSMutableAttributedString alloc] initWithString: reviewsText];
    
    [Utils addBoldAttributeToString: attrReviewsText forText: [NSString stringWithFormat: @"%ld", (long)_restaurant.numberOfReviews] font: _reviewsLabel.font.pointSize];
    _reviewsLabel.attributedText = attrReviewsText;
    
    [_reviewsLabel sizeToFit];
    CGRect reviewRect = _reviewsLabel.frame;
    CGRect starsRect = _starsView.frame;
    
    reviewRect.origin.x = CGRectGetMaxX(_pickupDetailView.frame) - CGRectGetWidth(reviewRect);
    reviewRect.size.height = starsRect.size.height;
    reviewRect.origin.y = starsRect.origin.y - 1.0f;
    _reviewsLabel.frame = reviewRect;
    starsRect.origin.x = CGRectGetMinX(reviewRect) - CGRectGetWidth(starsRect) - 10.0f;
    _starsView.frame = starsRect;
    
    _categoryButton.hidden = _restaurant.cuisineList.length == 0;
    [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
    _categoryButton.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    _categoryButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 2, 0);
}

- (IBAction)showAlert:(id)sender
{
    NSString *url = [DataManager discountURLForRestaurant: _restaurant.restaurantId];
    [CustomAlert showCustomAlertWithURL: url button: @"סגור"];
}
- (IBAction)showCusieneList:(UIButton*)sender
{
    CGRect buttonRect = _categoryButton.frame;
    CGFloat offset = CGRectGetWidth(self.frame) - CGRectGetMaxX(buttonRect);
    __weak RestaurantCell *weakSelf = self;
    if(_kosherIcon.hidden == NO) offset += 25;
    if(sender.selected == NO)
    {
        _initialCusineRect = _categoryButton.frame;
    }
    buttonRect.size.width = CGRectGetMaxX(buttonRect) - offset;
    buttonRect.origin.x = offset;
    [_categoryButton setTitle: @"  " forState: UIControlStateNormal];
    [UIView animateWithDuration: 0.2f animations:^{
        if (sender.selected)
        {
            _categoryButton.frame = _initialCusineRect;
            [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
        }
        else
        {
            _categoryButton.frame = buttonRect;
            [_categoryButton setTitle: _restaurant.cuisineList forState: UIControlStateNormal];
        }
        sender.selected = !sender.selected;
    }];
    
    if(sender.selected)
    {
        [Utils handleScreenTouch:^{
            [weakSelf showCusieneList:sender];
        }];
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    
}


- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
