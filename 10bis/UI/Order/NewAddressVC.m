//
//  NewAddressVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "NewAddressVC.h"
#import "DataManager.h"
#import "City.h"
#import "Street.h"
#import "Restaurant.h"
#import "RestaurantCell.h"
#import "SearchRestaurantVC.h"
#import "FrameAccessor.h"

@implementation NoAutoScrollUIScrollView
- (void)scrollRectToVisible:(CGRect)rect animated:(BOOL)animated
{
	// Don'd do anything here to prevent autoscrolling.
	// Unless you plan on using this method in another fashion.
}
@end

@interface NewAddressVC () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end
@implementation NewAddressVC
{
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UITextField *_cityTF;
    __weak IBOutlet UITextField *_streetTF;
    
    __weak IBOutlet UIScrollView *_scrollView;
    
    CGFloat _keyboardBottomInset;
    CGFloat _topInset;
    
    City *_selectedCity;
    Street *_selectedStreet;
    
    UIView *_searchView;
    UISearchBar *_searchBar;
    UITextField *_searchTF;
    
    CGRect _initialSearchRect;
    CGRect _initialTableRect;
    CGPoint _contentOffset;
    NSArray *_searchResults;
    __weak UITextField *_activeTF;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self)
    {
        self.title = [Utils screenTitleForDelivery: [DataManager deliveryMethod]];
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)back
{
    if (_popToRoot)
        [self.navigationController popToRootViewControllerAnimated: YES];
    else
        [self.navigationController popViewControllerAnimated: NO];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
   // [_searchView removeFromSuperview];
    _contentOffset = _scrollView.contentOffset;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    _scrollView.contentOffset = _contentOffset;
    if (CGRectEqualToRect(_initialTableRect, CGRectZero))
    {
        _initialTableRect = _tableView.frame;
    }
}
- (void)configureNavigationBar
{
    _initialSearchRect = CGRectMake(0, -64, DEVICE_WIDTH, 44);
    _searchView = [[UIView alloc] initWithFrame: _initialSearchRect];
    _searchView.backgroundColor = [UIColor clearColor];
    _searchBar = [[UISearchBar alloc] initWithFrame: _searchView.bounds];
    _searchBar.showsCancelButton = YES;
    _searchBar.backgroundImage = [UIImage alloc];
    _searchBar.tintColor = [Utils appRedColor];
    _searchBar.delegate = self;
    [_searchView addSubview: _searchBar];
    
    if ([Utils isIOS7] == NO)
    {
        _searchBar.backgroundColor = [Utils appRedColor];
    }
    
    
    UIButton *cancelButton = [UIButton buttonWithType: UIButtonTypeCustom];
    cancelButton.backgroundColor = [Utils appRedColor];
    [cancelButton setTitle: @"ביטול" forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor lightGrayColor] forState: UIControlStateHighlighted];
    
    [cancelButton addTarget: self action: @selector(cancelButtonClicked) forControlEvents: UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(250, 5, 65, 34);
    cancelButton.titleLabel.font = [UIFont fontWithName: @"Arial-BoldMT" size: 15];
    [_searchBar addSubview: cancelButton];
    _searchBar.tintColor = [Utils appRedColor];
    [self.navigationController.navigationBar addSubview: _searchView];
    
    for (UIView *subView in _searchBar.subviews){
        if ([subView isKindOfClass: [UITextField class]])
            _searchTF =  (UITextField *)subView;
        else
            for (UIView *secondLevelSubView in subView.subviews)
            {
                if ([secondLevelSubView isKindOfClass:[UITextField class]])
                {
                    _searchTF = (UITextField *)secondLevelSubView;
                    break;
                }
            }
        if (_searchTF)
        {
            _searchTF.enablesReturnKeyAutomatically = NO;
            _searchTF.textColor = [Utils appRedColor];
            _searchTF.background = [UIImage alloc];
            _searchTF.borderStyle = UITextBorderStyleNone;
            _searchTF.backgroundColor = RGB(228, 227, 223);
            _searchTF.layer.cornerRadius = 2.0f;
            
            if ([Utils isIOS7])
                _searchTF.tintColor = [Utils appRedColor];
            else
                [[_searchTF valueForKey:@"textInputTraits"] setValue: [Utils appRedColor] forKey:@"insertionPointColor"];
            break;
        }
    }
    
    [UIView animateWithDuration: 0.3f animations:^{
        CGRect searchRect = _searchView.frame;
        searchRect.origin.y = 0;
        _searchView.frame = searchRect;
    } completion:^(BOOL finished) {
        [_searchTF becomeFirstResponder];
    }];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _cityTF.leftView = _loadingView;

    _cityTF.rightView = [[UIView alloc] initWithFrame:_loadingView.frame];
    _streetTF.rightView = [[UIView alloc] initWithFrame:_loadingView.frame];
    _streetTF.leftView = _streetLoadingView;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
    {
    _streetTF.leftViewMode = UITextFieldViewModeAlways;
    _streetTF.rightViewMode = UITextFieldViewModeAlways;
    _cityTF.leftViewMode = UITextFieldViewModeAlways;
    _cityTF.rightViewMode = UITextFieldViewModeAlways;
    }

	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardShowed:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardHidden) name: UIKeyboardWillHideNotification object: nil];
    
    [_tableView registerNib: [UINib nibWithNibName: @"RestaurantCell" bundle: nil] forCellReuseIdentifier: @"RestaurantCell"];
    _scrollView.contentSize = _scrollView.frame.size;
    _tableView.backgroundColor = [UIColor clearColor];
   // [_cityTF layoutIfNeeded];
   // [_streetTF layoutIfNeeded];
    [_cityTF becomeFirstResponder];
    
//        if (_showSearchBar)
//        {
//            _tableView.frame = self.view.bounds;
//            _cityTF.hidden = YES;
//            [self configureNavigationBar];
//        }
}
- (void)updateTableInsets
{
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, _keyboardBottomInset, 0);
    _tableView.scrollIndicatorInsets = _tableView.contentInset;
    CGRect tableRect = _initialTableRect;
    tableRect.origin.y += _topInset;
    tableRect.size.height -= _topInset;
    [UIView animateWithDuration: 0.3f animations:^{
        _tableView.frame = tableRect;
    }];
}
- (void)keyboardShowed:(NSNotification*)notification
{
    NSValue* aValue     = [[notification userInfo] objectForKey: UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    _keyboardBottomInset = keyboardSize.height;
    [self updateTableInsets];
}
- (void)keyboardHidden
{
    _keyboardBottomInset = 0.0f;
    [self updateTableInsets];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"displayRestaurants"])
    {
        SearchRestaurantVC *searchVC = segue.destinationViewController;
        searchVC.restaurants = _searchResults;
        searchVC.addressString = [NSString stringWithFormat: @"%@, %@", _selectedCity.cityName, _selectedStreet.streetName];
        _searchResults = nil;
    }
}
#pragma mark - Search Bar
- (void)cancelButtonClicked
{
    [_searchBar resignFirstResponder];
    [UIView animateWithDuration: 0.3f animations:^{
        _searchView.frame = _initialSearchRect;
    }completion:^(BOOL finished) {
        [self back];
    }];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
#pragma mark - Text Field
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *result = [textField.text stringByReplacingCharactersInRange: range withString: string];
    if (result.length >= 2)
    {
        if (textField == _cityTF)
        {
            _selectedCity = nil;
            _selectedStreet = nil;
            _streetTF.text = nil;
            [_tfActivityIndicator startAnimating];
            [_streenTFActivityIndicator stopAnimating];
            _noResultsLabel.hidden = YES;
            [DataManager autocompleteCityName: result completion:^(id result, NSString *error) {
                if (textField != _activeTF)
                    return;
                if (error)
                    [Utils showErrorMessage: error];
                else
                {
                    _searchResults = result;
                    if(_searchResults.count == 0)
                    {
                        _noResultsLabel.hidden = NO;
                        _noResultsLabel.center = CGPointMake(self.view.frame.size.width/2, _activeTF.frame.origin.y  + _activeTF.frame.size.height + 20);
                    }
                    [_tableView reloadData];
                }
                [_tfActivityIndicator stopAnimating];

            }];
        }
        else if (textField == _streetTF)
        {
            _selectedStreet = nil;

           
            [_tfActivityIndicator stopAnimating];
            [_streenTFActivityIndicator startAnimating];
            _noResultsLabel.hidden = YES;
            [DataManager autocompleteStreetName: result parent: _selectedCity.cityId completion:^(id result, NSString *error) {
                if (textField != _activeTF)
                    return;
                if (error)
                    [Utils showErrorMessage: error];
                else
                {
                    _searchResults = result;
                    if(_searchResults.count == 0)
                    {
                        _noResultsLabel.hidden = NO;
                        _noResultsLabel.center = CGPointMake(self.view.frame.size.width/2, _activeTF.frame.origin.y  + _activeTF.frame.size.height + 20);
                    }
                    [_tableView reloadData];
                }
                [_streenTFActivityIndicator stopAnimating];
            }];
        }
    }
    else
    {
        if (textField == _cityTF)
        {
            _selectedCity = nil;
            _selectedStreet = nil;
            _streetTF.text = nil;
        }
        else if (textField == _streetTF)
        {
            _selectedStreet = nil;
        }
        _searchResults = nil;
        [_tableView reloadData];
    }
    
    return YES;
}
- (void)textFieldDidBeginEditing:(AppTextField *)textField
{
    _activeTF = textField;
    _topInset = 0.0f;
    [self updateTableInsets];
   // textField.filled = NO;
    
    if (textField == _cityTF)
    {
        
        [_scrollView setContentOffset: CGPointZero animated: YES];
        _searchResults = nil;
        [_tableView reloadData];
    }
    else if (textField == _streetTF)
    {
        [_scrollView setContentOffset: CGPointMake(0, 40) animated: YES];
    }
    [_tfActivityIndicator stopAnimating];
    
    [UIView animateWithDuration: 0.2f animations:^{
        _streetTF.alpha = textField == _cityTF ? 0.0f : 1.0f;
    }];
    [textField layoutIfNeeded];
}
- (void)textFieldDidEndEditing:(AppTextField *)textField
{
    [self updateFilledState: textField];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _cityTF && _selectedCity)
    {
        [_streetTF becomeFirstResponder];
    }
    
    if (_selectedCity && _selectedStreet)
    {
        [textField resignFirstResponder];
        [self searchRestaurants];
        
        return YES;
    }
    
    return NO;
}
- (void)updateFilledState:(AppTextField*)tf
{
  //  tf.filled = tf.text.length > 0;
    
}
#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _searchResults.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id searchItem = _searchResults[indexPath.section];
    if ([searchItem isKindOfClass: [Restaurant class]])
        return 90.0f;
    return 50.0f;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"SimpleCell";
    UITableViewCell *cell = nil;
    id searchItem = _searchResults[indexPath.section];
    
    if ([searchItem isKindOfClass: [Restaurant class]])
    {
        RestaurantCell *restaurantCell = (RestaurantCell*)[tableView dequeueReusableCellWithIdentifier: @"RestaurantCell"];
        restaurantCell.restaurant = searchItem;
        cell = restaurantCell;
    }
    else
        cell = [tableView dequeueReusableCellWithIdentifier: identifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: identifier];
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.textColor = [Utils appRedColor];
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName: @"Arial-BoldMT" size: 19];
        UIImageView *background = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"address_list_bg"]];
        [cell.contentView insertSubview: background atIndex: 0];
        [background sizeToFit];
        background.width = DEVICE_WIDTH;

        UIImageView *accessoryView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"icon_next"]];
        [accessoryView sizeToFit];
        cell.accessoryView = accessoryView;
    }
    
    if ([searchItem isKindOfClass: [City class]])
    {
        City *city = searchItem;
        cell.textLabel.text = city.cityName;
    }
    else if ([searchItem isKindOfClass: [Street class]])
    {
        Street *street = searchItem;
        cell.textLabel.text = street.streetName;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    if(_searchResults.count > indexPath.section)
    {
        if (_selectedCity == nil)
        {
            
            _selectedCity = _searchResults[indexPath.section];
            _cityTF.text = _selectedCity.cityName;
            [_cityTF layoutIfNeeded];
            _searchResults = nil;
            [_tableView reloadData];
            [_streetTF becomeFirstResponder];
        }
        else if (_selectedStreet == nil)
        {
            _selectedStreet = _searchResults[indexPath.section];
            if ([_selectedStreet isKindOfClass: [City class]] || [_selectedStreet isKindOfClass:[Restaurant class]])
            {
                return;
            }
            _streetTF.text = _selectedStreet.streetName;
            [_streetTF layoutIfNeeded];
            [_cityTF resignFirstResponder];
            [_streetTF resignFirstResponder];
            _searchResults = nil;
            [_tableView reloadData];
            
            [UIView animateWithDuration: 0.2f animations:^{
                _streetTF.alpha = 1.0f;
            }];
            
            [self searchRestaurants];
        }
    }
}

- (void)searchRestaurants
{
  //  [_scrollView setContentOffset: CGPointZero animated: YES];
  //  _topInset = 50.0f;
  //  [self updateTableInsets];
    
    __weak NewAddressVC *weakSelf = self;
    [self showLoadingOverlay];
    [DataManager searchRestaurantsWithCity: _selectedCity.cityId andStreet: _selectedStreet.streetId completion:^(NSArray* result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        NewAddressVC *strongSelf = weakSelf;
        if (strongSelf)
        {
            if (error)
                [Utils showErrorMessage: error];
            else
            {
                if (result.count == 0)
                {
                    [Utils showErrorMessage: @"לא נמצאו מסעדות"];
                    return;
                }
                strongSelf->_searchResults = result;
                //[strongSelf->_tableView reloadData];
                if (strongSelf.navigationController.topViewController == strongSelf)
                {
                    [DataManager setSearchCityID: _selectedCity.cityId];
                    [DataManager setSearchStreetID: _selectedStreet.streetId];
                    [strongSelf performSegueWithIdentifier: @"displayRestaurants" sender: strongSelf];
                }
            }
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2.0f;
}
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame: CGRectZero];
}
@end
