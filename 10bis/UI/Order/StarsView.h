//
//  StarsView.h
//  10bis
//
//  Created by Vadim Pavlov on 28.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarsView : UIView

@property (assign, nonatomic) NSInteger starsNum;
@property (assign, nonatomic) IBInspectable BOOL isBig;
@property (assign, nonatomic) BOOL isRTL;

- (void)setStars:(NSInteger)stars;
- (void)fitStarsToFrame;
- (void)enableSelection:(BOOL)enable;
@end
