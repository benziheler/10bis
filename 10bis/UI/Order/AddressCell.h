//
//  AddressCell.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Address;
@interface AddressCell : UITableViewCell
@property (nonatomic, strong) Address *address;
+ (CGFloat)heightForAddress:(Address*)address;
@end
