//
//  NewAddressVC.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@interface NewAddressVC : BaseVC
@property (nonatomic, strong) NSArray *searchResults;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *tfActivityIndicator;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *noResultsLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *streenTFActivityIndicator;
@property (nonatomic, assign) BOOL popToRoot;
@property (weak, nonatomic) IBOutlet UIView *streetLoadingView;
@end


@interface NoAutoScrollUIScrollView : UIScrollView
@end