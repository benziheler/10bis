//
//  StarsView.m
//  10bis
//
//  Created by Vadim Pavlov on 28.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "StarsView.h"
#import "FrameAccessor.h"

@implementation StarsView
{
    NSMutableArray *_starsViews;
    UIImageView *halfStarView;
    BOOL _selectionEnabled;
}
- (void)awakeFromNib
{
    _selectionEnabled = NO;
    _starsViews = [NSMutableArray arrayWithCapacity: 5];
    for (int i = 0; i < 5; i++)
    {
        NSString *greyName = _isBig ? @"rating_start_off" :  @"rate_star_grey";
        UIImageView *starView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: greyName]];
        NSString *redName = _isBig ? @"rating_start_on" :  @"rate_star_red";
        
        starView.highlightedImage = [UIImage imageNamed: redName];
        [starView sizeToFit];
        [self addSubview: starView];
        [_starsViews addObject:starView];
        CGRect starRect = starView.frame;
        CGFloat offset = 4.0f;
        starRect.origin.x = (CGRectGetWidth(starRect) + offset) * i;
        starView.frame = starRect;
    }
    
    halfStarView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"rate_star_half_red"]];
    [halfStarView sizeToFit];
    [self addSubview: halfStarView];
    halfStarView.hidden = YES;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    [self fitStarsToFrame];
    
}

- (void)fitStarsToFrame
{
    halfStarView.image = [UIImage imageNamed: @"rate_star_half_red"];

    if(_isRTL == NO)
    {
        CGFloat width = (self.width - 12)/5.0;
        NSInteger i = 0;
        for (UIImageView *starImgView in _starsViews)
        {
            starImgView.contentMode = UIViewContentModeScaleAspectFit;
            starImgView.size = CGSizeMake(width, self.height);
            starImgView.y = 0;
            starImgView.x = (width+6)*i;
            i++;
        }
        
        if(_starsNum%2)
        {
            UIImageView *starView = _starsViews[_starsNum/2];
            halfStarView.frame = starView.frame;
        }
    }
    else
    {
        UIImage *img = [UIImage imageWithCGImage:halfStarView.image.CGImage
                                           scale:halfStarView.image.scale
                                     orientation:UIImageOrientationUpMirrored];
        halfStarView.image = img;
        CGFloat width = (self.width - 12)/5.0;
        NSInteger i = 0;
        for (UIImageView *starImgView in _starsViews)
        {
            starImgView.contentMode = UIViewContentModeScaleAspectFit;
            starImgView.size = CGSizeMake(width, self.height);
            starImgView.y = 0;
            starImgView.x = self.width - (width*(i+1))-6;
            i++;
        }
        
        if(_starsNum%2)
        {
            UIImageView *starView = _starsViews[_starsNum/2];
            halfStarView.frame = starView.frame;
        }

    }
}

- (void)setStars:(NSInteger)stars
{
    self.starsNum = stars;
    halfStarView.hidden = YES;
    for (int i = 1; i < 6; i++)
    {
        UIImageView *starView = _starsViews[i-1];
        if (i * 2 <= stars)
        {
            starView.highlighted = YES;
        }
        else if (i * 2 - 1 == stars)
        {
            starView.highlighted = NO;
            halfStarView.hidden = NO;
            halfStarView.frame = starView.frame;
        }
        else
            starView.highlighted = NO;
    }
}

- (void)enableSelection:(BOOL)enable
{
    [halfStarView removeFromSuperview];
    halfStarView = nil;
    _selectionEnabled = enable;
    if(_selectionEnabled)
    {
        for (UIImageView *imgView in _starsViews) {
            imgView.userInteractionEnabled = YES;
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_selectionEnabled)
    {
        UITouch *touch = event.allTouches.anyObject;
        NSInteger index = [_starsViews indexOfObject:touch.view];
        [self setStars:((index+1) * 2)];
    }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_selectionEnabled)
    {
        UITouch *touch = event.allTouches.anyObject;
        CGPoint point = [touch locationInView:self];
        NSInteger index = point.x / (self.width / 5) + 1;
        [self setStars:index *2];
    }
}

@end
