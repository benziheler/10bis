//
//  BaseVC.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController

- (void)showValidationAlertFor:(UITextField*)tf;

- (void)showLoadingOverlay;
- (void)removeLoadingOverlay;

- (void)showCustomAlert:(NSString*)title msg:(NSString*)message button:(NSString*)btn alignment:(NSTextAlignment)alignment completion:(dispatch_block_t)handler;
- (void)showCustomAlertWithURL:(NSString*)url button:(NSString*)btn;
@end
