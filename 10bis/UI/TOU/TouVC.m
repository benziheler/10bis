//
//  TouVC.m
//  10bis
//
//  Created by Vadim Pavlov on 11.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "TouVC.h"

@interface TouVC ()

@end

@implementation TouVC
{
    __weak IBOutlet UIWebView *_webView;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *url = @"http://www.10bis.co.il/Info/TermOfUse?isMobileUser=true";
    [_webView loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: url]]];

}

@end
