//
//  TransactionCell.h
//  10bis
//
//  Created by Anton Vilimets on 9/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKSTableViewCell.h"

@interface TransactionCell : SKSTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icnImageView;
- (void)setupWithDict:(NSDictionary *)dict;

@end
