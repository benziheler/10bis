//
//  ReportViewController.h
//  10bis
//
//  Created by Anton Vilimets on 9/29/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseNibView.h"
#import "StarsView.h"

@interface ReviewView : BaseNibView <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet AppRedButton *sendButton;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet StarsView *starsView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentVerticalSpaceConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentBottomSpaceConstraint;
@property (copy, nonatomic) void (^sendReviewCompletion)(NSInteger reviewId, NSNumber *transactionId, NSString *comment, NSNumber *rank);



@property (weak, nonatomic) IBOutlet UIView *contentView;
- (void)sendReviewWithObj:(NSDictionary *)object;
- (void)showReviewWithObj:(NSDictionary *)object;
- (IBAction)closePressed:(id)sender;
- (IBAction)sendPressed:(id)sender;

@end
