//
//  TransactionActionCell.m
//  10bis
//
//  Created by Anton Vilimets on 9/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "TransactionActionCell.h"
#import "FrameAccessor.h"


@implementation TransactionActionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    _orderButton.layer.cornerRadius = _reviewButton.layer.cornerRadius = 3;
}


- (void)setupWithDict:(NSDictionary *)dict
{

    BOOL orderHidden = YES;
    BOOL reviewHidden = YES;
   orderHidden = [dict[@"TransactionType"] isEqualToString:@"terminal"];
    if([dict[@"ReviewId"] integerValue] != 0)
    {
        [_reviewButton setTitle:@"הצג דירוג" forState:UIControlStateNormal];
        reviewHidden = NO;
    }
    else if([dict[@"AddReviewDisabled"] boolValue] == NO)
    {
        [_reviewButton setTitle:@"כתוב דירוג" forState:UIControlStateNormal];
        reviewHidden = NO;
    }
    else
    {
        reviewHidden = YES;
    }
    
    if(reviewHidden)
    {
        [_orderButton setCenterX:self.width/2];
    }
    else
    {
        if(!orderHidden)
        {
            CGFloat space = (DEVICE_WIDTH - _orderButton.width*2)/3;
            [_orderButton setX:space * 2 + _reviewButton.width];
            [_reviewButton setX:space];
        }
        else
        {
            [_reviewButton setCenterX:self.width/2];
        }
    }
    [self layoutIfNeeded];
    _orderButton.hidden = orderHidden;
    _reviewButton.hidden = reviewHidden;
   }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
