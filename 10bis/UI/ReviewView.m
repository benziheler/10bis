//
//  ReportViewController.m
//  10bis
//
//  Created by Anton Vilimets on 9/29/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "ReviewView.h"
#import "NSDate+Helpers.h"
#import "FrameAccessor.h"

#define  kCaptionText @"כתוב משוב/המלצה"

@interface ReviewView ()
{
    NSDictionary *currentObj;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint;



@end

@implementation ReviewView


- (void)awakeFromNib
{
    [super awakeFromNib];
    self.widthConstraint.constant = DEVICE_WIDTH - 40;
    _sendButton.layer.cornerRadius = 3;
    [self setNeedsUpdateConstraints];
    [self layoutIfNeeded];
}

- (void)sendReviewWithObj:(NSDictionary *)object
{
    
    currentObj = object;
    [self baseSetupWithObj:object date:[NSDate date]];
    _textView.editable = YES;
    _textView.text = kCaptionText;
    
    _sendButton.hidden = NO;
    _textViewHeightConstraint.constant = 134;
    _textViewBottomConstraint.constant = 74;
    [self.textView setNeedsUpdateConstraints];
    [_starsView enableSelection:YES];
    [_starsView setStars:0];
    [self show];
}

- (void)showReviewWithObj:(NSDictionary *)object
{
    currentObj = object;
    NSString *timestamp = [object[@"ReviewDate"] stringByReplacingOccurrencesOfString:@"/Date(" withString:@""];
    timestamp = [timestamp stringByReplacingOccurrencesOfString:@")/" withString:@""];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp.doubleValue];
    [self baseSetupWithObj:object date:date];
    _textView.text = object[@"ReviewText"];
    _textViewHeightConstraint.constant = 200;
    if([UIScreen mainScreen].bounds.size.width)
    {
        _textViewHeightConstraint.constant = 150;

    }
    _textViewBottomConstraint.constant = 1;
    [self.textView setNeedsUpdateConstraints];
    [_starsView enableSelection:NO];
    [_starsView setStars:[object[@"ReviewRank"] integerValue]];
    
    _textView.editable = NO;
    _sendButton.hidden = YES;
    [self show];
}

- (void)baseSetupWithObj:(NSDictionary *)object date:(NSDate *)date
{
    [_starsView fitStarsToFrame];
    _nameLabel.text = object[@"ResName"];
    _dateLabel.text = [date dateStringWithFormat:@"hh:mm dd/M/yyyy"];
    [self loadImage:object[@"ResLogoUrl"]];
    
    if([object[@"TransactionType"] isEqualToString:@"terminal"])
    {
        _typeLabel.text = @"תשלום במסעדה";
    }
    else if([object[@"TransactionType"] isEqualToString:@"websiteorder"])
    {
        _typeLabel.text = @"הזמנת משלוח";
    }
    else if([object[@"TransactionType"] isEqualToString:@"pickup"])
    {
        _typeLabel.text = @"הזמנת פיקאפ";
    }
}

- (void)loadImage:(NSString *)url
{
    [Utils loadImage:url completion:^(UIImage *image) {
        if (image)
        {
            _imageView.image = image;
        }
    }];
    
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    _contentVerticalSpaceConstraint.constant -= 220;
    _contentBottomSpaceConstraint.constant += 220;
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    }];
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    _contentVerticalSpaceConstraint.constant = 30;
    _contentBottomSpaceConstraint.constant = 30;
    [self setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    }];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
 
    if([textView.text isEqual:kCaptionText])
    {
        textView.text =@"";
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.text = kCaptionText;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (IBAction)closePressed:(id)sender
{
    [self hide];
}

- (IBAction)sendPressed:(id)sender
{
    [_textView resignFirstResponder];
    if(_starsView.starsNum/2 > 1)
    {
        __weak ReviewView *weakSelf = self;
        NSString *text = _textView.text;
        if([text isEqualToString:kCaptionText]) text = @"";
        [DejalBezelActivityView activityViewForView:self];
        [DataManager sendReview:_starsView.starsNum/2 text:text transaction:currentObj completion:^(id result, NSString *error) {
            [DejalBezelActivityView removeViewAnimated:YES];
            
            if(result)
            {
                if(_sendReviewCompletion)
                {
                    _sendReviewCompletion([result integerValue], currentObj[@"TransactionId"], text,@(weakSelf.starsView.starsNum));
                }
                [self hide];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"אנא דרג את המסעדה" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)show
{
    id <UIApplicationDelegate> delegate = [UIApplication sharedApplication].delegate;
    [delegate.window addSubview:self];
    [self setNeedsUpdateConstraints];
    [self layoutIfNeeded];
    self.alpha = 0;
    self.hidden = NO;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
    {
    _contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.0f, 0.0f);
    _contentView.hidden = YES;
    [UIView animateWithDuration:0.15 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        self.contentView.alpha = 0;
        _contentView.hidden = NO;
        [UIView animateWithDuration:0.1 animations:^{self.contentView.alpha = 1.0;}];
        
        self.contentView.layer.transform = CATransform3DMakeScale(0.5, 0.5, 1.0);
        
        CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
        bounceAnimation.values = [NSArray arrayWithObjects:
                                  [NSNumber numberWithFloat:0.5],
                                  [NSNumber numberWithFloat:1.2],
                                  [NSNumber numberWithFloat:0.9],
                                  [NSNumber numberWithFloat:1.0], nil];
        bounceAnimation.duration = 0.4;
        bounceAnimation.removedOnCompletion = NO;
        [self.contentView.layer addAnimation:bounceAnimation forKey:@"bounce"];
        
        self.contentView.layer.transform = CATransform3DIdentity;
    }];
    }
    else
    {
        self.alpha = 1;
        self.contentView.hidden = NO;
        _contentView.alpha = 0;
        [UIView animateWithDuration:0.2 animations:^{
            _contentView.alpha = 1;
        }];

    }
    
}

- (void)hide
{
    [self endEditing:YES];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
    {
    [UIView animateWithDuration:0.2 animations:^{
        
        _contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.0f, 0.0f);
        
    } completion:^(BOOL finished) {
        _contentView.hidden = YES;
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
            self.alpha = 1;
        }];
    }];
    }
    else
    {
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
            self.alpha = 1;
        }];
    }
}


@end
