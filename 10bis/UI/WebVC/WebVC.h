//
//  WebVC.h
//  10bis
//
//  Created by Vadim Pavlov on 04.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@class Dish;
@interface WebVC : BaseVC
@property (nonatomic, assign) BOOL showNavigationBar;
@property (nonatomic, strong) NSString *urlToLoad;
@end
