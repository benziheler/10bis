//
//  TableFor2VC.m
//  10bis
//
//  Created by Anton Vilimets on 6/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "TableFor2VC.h"
#import "AppDelegate.h"

@interface TableFor2VC ()
{
    BOOL alreadyLoaded;
}

@end

@implementation TableFor2VC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate) name:UIDeviceOrientationDidChangeNotification object:nil];
    [self reloadWebView];
    [self.bigWebView setAllowsInlineMediaPlayback:YES];
    [self.bigWebView setMediaPlaybackRequiresUserAction:NO];
       NSURLRequest* request = [NSURLRequest requestWithURL: [NSURL URLWithString: [DataManager tableForTwoURL]]];
    _bigWebView.delegate = self;
    [_bigWebView loadRequest:request];
    _bigWebView.hidden = YES;
}

- (void)reloadWebView
{
    NSString *embedCode = @"<body style='margin:0px;padding:0px;'><iframe width=\"240\" height=\"210\"  src=\"http://www.youtube.com/embed/HieorPee6KM\" frameborder=\"0\" allowfullscreen style='margin-top:-30px;'></iframe></body>";
    [_ytWebView loadHTMLString:embedCode baseURL:nil];
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    CGRect frame = _joinButton.frame;
    frame.size.width = 240;
    frame.origin.x = 40;
    frame.size.height = 40;
    _joinButton.frame = frame;
    [self reloadWebView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
  //  AppDelegate *appDlg = [UIApplication sharedApplication].delegate;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = request.URL.absoluteString;
    NSString *backPrefix = @"tenbis://back";
    NSString *homePrefix = @"tenbis://home";
    
    if ([requestString isEqualToString: backPrefix])
    {
        [self.navigationController popViewControllerAnimated: YES];
        return NO;
    }
    else if ([requestString isEqualToString: homePrefix])
    {
        [DataManager loginOnInit];
        [self.navigationController popToRootViewControllerAnimated: YES];
        return NO;
    }
    else if ([requestString hasPrefix: backPrefix])
    {
        // TODO: extract encryptedUserId and login
        [self.navigationController popViewControllerAnimated: YES];
        return NO;
    }
    else if ([requestString hasPrefix: homePrefix])
    {
        // TODO: extract encryptedUserId and login
        [self.navigationController popToRootViewControllerAnimated: YES];
        return NO;
    }
    else if ([requestString hasSuffix:@"TableForTwoRegistration"])
    {
        if(alreadyLoaded)
        {
        [self.navigationController popToRootViewControllerAnimated: YES];
        return NO;
        }
        else
        {
            alreadyLoaded = YES;
        }
    }
    
    return YES;
}

- (IBAction)joinPressed:(id)sender
{
    _bigWebView.alpha = 0;
    _bigWebView.hidden = NO;
    [UIView animateWithDuration:0.6 animations:^{
        _joinButton.alpha = 0;
        _ytWebView.alpha = 0;
        _bigWebView.alpha = 1;
        _topImageView.alpha = 0;
        _topLabel.alpha = 0;
    }];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}
@end
