//
//  TableFor2VC.h
//  10bis
//
//  Created by Anton Vilimets on 6/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@interface TableFor2VC : BaseVC <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *ytWebView;
@property (weak, nonatomic) IBOutlet UIWebView *bigWebView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerIndicator;
@property (weak, nonatomic) IBOutlet UIButton *joinButton;
- (IBAction)joinPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;

@end
