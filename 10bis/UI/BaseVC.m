//
//  BaseVC.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"

#import "CustomAlert.h"
@interface BaseVC ()

@end

@implementation BaseVC
{
    DejalBezelActivityView *_loadingView;
    __weak UITextField *_invalidTF;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"bg"]];
    backgroundView.frame = self.view.bounds;
    backgroundView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self.view insertSubview: backgroundView atIndex: 0];
    if (self.navigationItem.leftBarButtonItem == nil)
    {
        UIButton *backButton = [UIButton buttonWithType: UIButtonTypeCustom];
        [backButton setImage: [UIImage imageNamed: @"icon_back"] forState: UIControlStateNormal];
        if ([Utils isIOS7])
        {
            [backButton sizeToFit];
        }
        else
        {
            backButton.frame = CGRectMake(0, 0, 30, 25);
        }
        [backButton addTarget: self action: @selector(back) forControlEvents: UIControlEventTouchUpInside];
        UIBarButtonItem *backArrow = [[UIBarButtonItem alloc] initWithCustomView: backButton];
        [self.navigationItem setLeftBarButtonItem: backArrow animated: YES];
    }
}

- (void)back
{
    [self.navigationController popViewControllerAnimated: YES];
}
- (void)showLoadingOverlay
{
    [DejalBezelActivityView activityViewForView: self.view withLabel: @""];
}
- (void)removeLoadingOverlay
{
    [DejalBezelActivityView removeViewAnimated: NO];
}
- (void)showCustomAlert:(NSString*)title msg:(NSString*)message button:(NSString*)btn alignment:(NSTextAlignment)alignment completion:(dispatch_block_t)handler
{
    [CustomAlert showCustomAlert: title msg: message button: btn alignment: alignment completion: handler];
}
- (void)showCustomAlertWithURL:(NSString*)url button:(NSString*)btn
{
    [CustomAlert showCustomAlertWithURL: url button: btn];
}
- (void)showValidationAlertFor:(UITextField*)tf
{
    _invalidTF = tf;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message: @"נא מלא את הפרטים הנחוצים" delegate: self cancelButtonTitle: @"אישור" otherButtonTitles: nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [_invalidTF becomeFirstResponder];
    _invalidTF = nil;
}

@end
