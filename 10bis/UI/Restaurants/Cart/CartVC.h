//
//  CartVC.h
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@class OrderConfirmation;
@interface CartVC : BaseVC
@property (nonatomic, strong) OrderConfirmation *order;
@property (nonatomic, assign) NSInteger minOrder;
@property (nonatomic, assign) BOOL companyFlag;
- (IBAction)continueOrder:(id)sender;
@end
