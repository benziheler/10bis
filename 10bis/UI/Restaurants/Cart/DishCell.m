//
//  DishCell.m
//  10bis
//
//  Created by Vadim Pavlov on 14.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "DishCell.h"
#import "DishConfirmation.h"
@implementation DishCell
{
    __weak IBOutlet UILabel *_priceLabel;
    __weak IBOutlet UILabel *_userLabel;
    __weak IBOutlet UILabel *_nameLabel;
    __weak IBOutlet UILabel *_quantityLabel;
    __weak IBOutlet UIView *_priceVIew;
}
- (void)awakeFromNib
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.backgroundView = [UIView new];
    _priceVIew.backgroundColor = [Utils appRedColor];
    _priceVIew.layer.cornerRadius = 2;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setDish:(DishConfirmation *)dish
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _dish = dish;
    _priceLabel.text = [NSString stringWithFormat: @"₪ %@", dish.totalDishPrice];
    _nameLabel.text = dish.dishName;
    _userLabel.text = dish.dishUsername;
    _quantityLabel.text = [NSString stringWithFormat: @"X %ld", (long)dish.quantity];
    if(_dish.dishUsername.length > 0)
    {
        _quantityLabel.y = _nameLabel.y = 6;
    }
    else
    {
        _quantityLabel.y = _nameLabel.y = self.height/2 - _nameLabel.height/2;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];    
    if ([Utils isIOS7] == NO)
    {
        // ios6 group table inset fix
        CGRect r = self.contentView.frame;
        r.origin.x = 0.0f;
        r.size.width = DEVICE_WIDTH;
        r.size.height = self.frame.size.height;
        self.contentView.frame = r;
    }
}
@end
