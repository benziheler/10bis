//
//  RestaurantMenuVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantMenuVC.h"
#import "Restaurant.h"
#import "MenuHeader.h"
#import "MenuCell.h"
#import "DataManager.h"
#import "Menu.h"
#import "Dish.h"
#import "WebVC.h"
#import "DishVC.h"
#import "StarsView.h"
#import "GradientView.h"
#import "CartVC.h"
#import "RestaurantCell.h"
#import "FrameAccessor.h"

@class OrderConfirmation;
@interface RestaurantMenuVC () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) RestaurantCell *cell;

@end

@implementation RestaurantMenuVC
{

    __weak IBOutlet UIImageView *_logoView;
    __weak IBOutlet UITableView *_tableView;

    __weak IBOutlet UIButton *_discountButton;
    
    __weak IBOutlet UILabel *_noMenuLabel;
    NSArray *_menu;
    NSMutableSet *_openedCategories;
    
    Dish *_selectedDish;
    
    BOOL _isLoading;
    
    DishVC *_dishScreen;
    __weak IBOutlet UIButton *_orderButton;
    __weak IBOutlet GradientView *_orderView;
    
    CartVC *_cartVC;
    OrderConfirmation *_order;
}
- (void)setRestaurant:(Restaurant *)restaurant
{
    _restaurant = restaurant;
    _isLoading = YES;
    __weak RestaurantMenuVC *weakSelf = self;
    [DataManager getRestaurantMenu: restaurant.restaurantId userId: nil completion:^(id result, NSString *error) {
        RestaurantMenuVC *strongSelf = weakSelf;
        if (strongSelf)
        {
            strongSelf->_isLoading = NO;
            [strongSelf removeLoadingOverlay];
            if (error)
                [Utils showErrorMessage: error];
            else
            {
                strongSelf->_menu = result;
                strongSelf->_openedCategories = [NSMutableSet setWithCapacity: strongSelf->_menu.count];
                [strongSelf->_tableView reloadData];
                if ([DataManager deliveryMethod] != DeliveryMethodSitting)
                {
                    [strongSelf updateCart];
                }
                
                if ([DataManager deliveryMethod] == DeliveryMethodSitting && _menu.count > 0)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"התפריט המוצג הינו תפריט הפיק-אפ של המסעדה. ייתכנו שינויים במחירי המנות בעת ישיבה במסעדה." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
                
                _noMenuLabel.hidden = !(_menu.count == 0);
            }
        }
    }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *name = NSStringFromClass([RestaurantCell class]);
    NSArray *xib = [[NSBundle mainBundle] loadNibNamed:name  owner: self options: nil];
    self.cell = xib[0];
    _cell.backgroundColor = [UIColor clearColor];
    _cell.contentView.backgroundColor = [UIColor clearColor];
    _cell.width = DEVICE_WIDTH + 15;
    [self.view addSubview:_cell];
    [_cell setRestaurant:_restaurant];
    _cell.bgImageView.hidden = YES;

    _cell.arrowImgView.hidden = YES;
    _noMenuLabel.textColor = [Utils appRedColor];
    if (_isLoading)
        [self showLoadingOverlay];
    
    DeliveryMethod method = [DataManager deliveryMethod];

    NSString *screenTitle = nil;
    switch (method) {
        case  DeliveryMethodDelivery:
            screenTitle = [NSString stringWithFormat: @"משלוח - %@", _restaurant.name];
            break;
        case DeliveryMethodPickup:
            screenTitle = [NSString stringWithFormat: @"פיק אפ - %@", _restaurant.name];
            break;
        case DeliveryMethodSitting:
            screenTitle = [NSString stringWithFormat: @"תפריט - %@", _restaurant.name];
            break;
        default:
            break;
    }
    _orderButton.layer.cornerRadius = 3.0f;
	// Do any additional setup after loading the view.
    UILabel *titleLabel = [[UILabel alloc]initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 30)];
    NSDictionary *attrs = [[UINavigationBar appearance] titleTextAttributes];
    titleLabel.font = attrs[NSFontAttributeName];
    titleLabel.textColor = attrs[NSForegroundColorAttributeName];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = screenTitle; //[NSString stringWithFormat: @"תפריט - %@", _restaurant.name];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UIView *titleWrapView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, DEVICE_WIDTH, 40)];
    [titleWrapView addSubview: titleLabel];
    titleWrapView.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = titleWrapView;
    
 
    if (_curLocation.longitude != 0 & _curLocation.latitude != 0)
        [self updateLocation];
        _discountButton.layer.cornerRadius = 3.0f;
    _logoView.layer.cornerRadius = 2.0f;
    _logoView.layer.masksToBounds = YES;
    
    BOOL couponHidden = NO;
    NSString *couponText = nil;
    switch (method) {
        case DeliveryMethodDelivery:
            couponHidden = _restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)_restaurant.discountCouponPercent];
            break;
        case DeliveryMethodPickup:
            couponHidden = _restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)_restaurant.discountCouponPercent];
            break;
        case DeliveryMethodSitting:
            if (_restaurant.isHappyHourActive == NO ||
                [_restaurant.happyHourDiscount isEqualToString: @"0%"])
            {
                couponHidden = YES;
            }
            couponText = _restaurant.happyHourDiscount;
            break;
        default:
            break;
    }
    
    // discount
    _discountButton.hidden = YES;//couponHidden;
    if ((method == DeliveryMethodSitting || _restaurant.couponHasRestrictions) && couponText)
        couponText = [@"* " stringByAppendingString: couponText];    
       [_discountButton setTitle: couponText forState: UIControlStateNormal];
    
    
    
    [Utils loadImage: _restaurant.logoURL completion:^(UIImage *image) {
        _logoView.image = image;
    }];
    
    [_tableView registerNib: [UINib nibWithNibName: @"MenuCell" bundle: nil] forCellReuseIdentifier: @"MenuCell"];
    [_tableView registerNib: [UINib nibWithNibName: @"MenuHeader" bundle: nil] forHeaderFooterViewReuseIdentifier: @"MenuHeader"];
    
    if (method == DeliveryMethodSitting)
        [self hideToolbar];
    else
    {
        _orderView.colors = @[[UIColor clearColor], [UIColor whiteColor]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(cartClicked:)];
        [_orderView addGestureRecognizer: tap];
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if ([DataManager deliveryMethod] != DeliveryMethodSitting)
    {
        [self updateCart];
    }
    

}
- (void)setCurLocation:(CLLocationCoordinate2D)curLocation
{
    _curLocation = curLocation;
    if (self.isViewLoaded)
    {
        [self updateLocation];
    }
}
- (void)updateLocation
{
    if (_curLocation.longitude != 0 & _curLocation.latitude != 0)
    {
        CLLocation *restaurantLocation = [[CLLocation alloc] initWithLatitude: _restaurant.latitude longitude: _restaurant.longitude];
        CLLocation *userLocation = [[CLLocation alloc] initWithLatitude: _curLocation.latitude longitude: _curLocation.longitude];
        
        CLLocationDistance distance = [userLocation distanceFromLocation: restaurantLocation] / 1000;
        _cell.distanceLabel.text = [NSString stringWithFormat: @"      %0.2f %@", distance, @"ק״מ ממך"];
    }
}
- (void)hideToolbar
{
    CGRect tableRect = _tableView.frame;
    tableRect.size.height += 48.0f;
    _tableView.frame = tableRect;
    _orderView.hidden = YES;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"dishSegue"])
    {
        DishVC *dishVC = segue.destinationViewController;
        dishVC.restaurant = _restaurant;
        dishVC.dish = _selectedDish;
        dishVC.didAddDish = ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                            [self didAddDish];
            });

        };
    }
}
- (NSArray*)dishesForCategory:(NSInteger)categoryIndex
{
    Menu *menu = _menu[categoryIndex];
    return menu.dishList;
}
- (void)didAddDish
{
    [self updateCart];
    _dishScreen = [self.storyboard instantiateViewControllerWithIdentifier: @"DishVC"];
    _dishScreen.restaurant = _restaurant;
    [self addChildViewController:_dishScreen];
    [_dishScreen didMoveToParentViewController:self];
    _dishScreen.dish = _selectedDish;
    
    [self.view addSubview: _dishScreen.view];
    _dishScreen.view.frame = self.view.bounds;
    [self.navigationController popViewControllerAnimated:NO];
    UIView *view = _dishScreen.view;
    [UIView animateWithDuration: 0.4f animations:^{
        view.transform = CGAffineTransformScale(view.transform, 0.1f, 0.03f);
        CGRect buttonRect = [self.view convertRect: _orderButton.frame fromView: _orderView];
        view.x = buttonRect.origin.x;
        view.y = buttonRect.origin.y;

    } completion:^(BOOL finished) {
        [view removeFromSuperview];
    }];
    [UIView animateWithDuration:0.2 delay:0.2 options:0 animations:^{
        view.alpha = 0.5;
    } completion:^(BOOL finished) {
        
    }];

}
#pragma mark - Actions
- (void)updateCart
{
    [DataManager shoppingCartTotalForRestaurant: _restaurant.restaurantId completion:^(id result, NSString *error) {
        if (error)
        {
            [Utils showErrorMessage: error];
        }
        else if ([result isKindOfClass: [NSString class]])
        {
            if ([result isEqualToString: @"-1"])
            {
                result = @"0";
            }
            [_orderButton setTitle: [NSString stringWithFormat: @"%@", result] forState: UIControlStateNormal];
        }
    }];

}
- (void)cartClicked:(id)sender
{
    [self showLoadingOverlay];
    __weak RestaurantMenuVC *weakSelf = self;
    
    [DataManager orderConfirmation:^(id result, NSString *error) {
        RestaurantMenuVC *strongSelf = weakSelf;

        if (strongSelf)
        {
            [strongSelf removeLoadingOverlay];
            if(strongSelf.navigationController.topViewController == strongSelf &&
               result)
            {
                strongSelf->_order = result;
                [strongSelf transitToCart];
            }
        }
        else if (error)
            [Utils showErrorMessage: error];
    }];
}
- (void)transitToCart
{
    id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:@"Shopping Cart"
                                                           value:nil] build]];

    _cartVC = [self.storyboard instantiateViewControllerWithIdentifier: @"CartVC"];
    _cartVC.order = _order;
    _cartVC.minOrder = _restaurant.minimumPriceForOrder;
    _cartVC.companyFlag = _restaurant.companyFlag;
    
    UIView *cartView = _cartVC.view;
    cartView.transform = CGAffineTransformScale(cartView.transform, 0.1f, 0.1f);
    cartView.alpha = 0.5f;
    CGRect buttonRect = [self.view convertRect: _orderButton.frame fromView: _orderView];
    cartView.frame = buttonRect;
    [self.view addSubview: cartView];
    [UIView animateWithDuration: 0.4f animations:^{
        cartView.transform = CGAffineTransformIdentity;
        cartView.alpha = 1.0f;
        cartView.frame = self.view.bounds;
    } completion:^(BOOL finished) {
        [cartView removeFromSuperview];
        [self.navigationController pushViewController: _cartVC animated: NO];
    }];

}
- (IBAction)showDiscountAlert:(id)sender
{
    NSString *url = [DataManager discountURLForRestaurant: _restaurant.restaurantId];
    [self showCustomAlertWithURL: url button: @"סגור"];
}


#pragma mark - Table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _menu.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *dishes = [self dishesForCategory: section];
    BOOL isSectionOpened = [_openedCategories containsObject: @(section)];
    
    return isSectionOpened ? dishes.count : 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *cell = (MenuCell*)[tableView dequeueReusableCellWithIdentifier: @"MenuCell"];
    NSArray *dishes = [self dishesForCategory: indexPath.section];
    Dish *dish = dishes[indexPath.row];
    cell.dish = dish;
    
    DeliveryMethod method = [DataManager deliveryMethod];
    cell.arrowImageView.hidden = method == DeliveryMethodSitting;
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MenuHeader *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier: @"MenuHeader"];
    header.backgroundView = [[UIView alloc] initWithFrame: CGRectZero];
    header.backgroundView.backgroundColor = [UIColor clearColor];
    Menu *category = _menu[section];
    header.headerLabel.text = category.categoryName;
    header.isOpen = [_openedCategories containsObject: @(section)];
    header.section = section;
    
    header.didClickOnHeader = ^(MenuHeader *clickedHeader){
        
        NSArray *dishes = [self dishesForCategory: clickedHeader.section];
        NSMutableArray *cellsToAnimate = [NSMutableArray arrayWithCapacity: dishes.count];
        for (int i = 0; i < dishes.count; i++)
        {
            [cellsToAnimate addObject: [NSIndexPath indexPathForRow: i inSection: section]];
        }
        UITableViewRowAnimation animation = UITableViewRowAnimationNone;
        [_tableView beginUpdates];
        if ([_openedCategories containsObject: @(section)])
        {
            if(section == 0)
            {
                [_tableView setContentOffset:CGPointMake(0, 0) animated:YES];
            }
            [_openedCategories removeObject: @(section)];
            [_tableView deleteRowsAtIndexPaths:cellsToAnimate withRowAnimation: animation];
           
        }
        else
        {
            [_openedCategories addObject: @(section)];
            [_tableView insertRowsAtIndexPaths:cellsToAnimate withRowAnimation: animation];
                    }
        
        
        
        [_tableView endUpdates];

    };
    return header;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    Menu *category = _menu[section];    
    return [MenuHeader heightForText:category.categoryName];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dishes = [self dishesForCategory: indexPath.section];
    Dish *dish = dishes[indexPath.row];
    CGFloat cellHeight = [MenuCell heightForDish: dish];
    return cellHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    DeliveryMethod method = [DataManager deliveryMethod];
    if (method == DeliveryMethodSitting)
        return;
    
    Menu *category = _menu[indexPath.section];
    Dish *dish = category.dishList[indexPath.row];
    
    __weak RestaurantMenuVC *weakSelf = self;
    [self showLoadingOverlay];
    [DataManager getDetailedDish: dish.dishId categoryId: dish.categoryId completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        RestaurantMenuVC *strongSelf = weakSelf;
        if (result && strongSelf)
        {
            strongSelf->_selectedDish = result;
            if (strongSelf.navigationController.topViewController == strongSelf)
            {
                [strongSelf performSegueWithIdentifier: @"dishSegue" sender: self];
            }
        }
        else
        {
            [Utils showErrorMessage: error];
        }
    }];

//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message: @"לא פעיל עדיין" delegate: Nil cancelButtonTitle: @"אישור" otherButtonTitles: nil];
//    [alert show];

}
@end
