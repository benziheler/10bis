//
//  MenuHeader.h
//  10bis
//
//  Created by Vadim Pavlov on 24.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuHeader : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, copy) void (^didClickOnHeader)(MenuHeader* clickedHeader);

@property (nonatomic, assign) CGFloat headerHeight;
+ (CGFloat)heightForText:(NSString*)headerText;
+ (CGFloat)heightForAttrString:(NSAttributedString *)attrString;
@end
