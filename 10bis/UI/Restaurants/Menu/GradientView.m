//
//  GradientView.m
//  10bis
//
//  Created by Vadim Pavlov on 25.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "GradientView.h"
#import "ACCoreGraphicsBasics.h"
@implementation GradientView
- (void)setColors:(NSArray *)colors
{
    _colors = colors;
}
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    UIColor *color1 = _colors[0];
    UIColor *color2 = _colors[1];
    drawGradientOfTwoColors(ctx, rect,  color1.CGColor, color2.CGColor, 5);
}
@end
