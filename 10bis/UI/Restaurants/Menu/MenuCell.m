//
//  MenuCell.m
//  10bis
//
//  Created by Vadim Pavlov on 24.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "MenuCell.h"
#import "Dish.h"
@implementation MenuCell
{
    __weak IBOutlet UILabel *_titleLabel;
    __weak IBOutlet UILabel *_subTitleLabel;
    __weak IBOutlet UILabel *_priceLabel;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
 
    _priceView.backgroundColor = [Utils appRedColor];
    _priceView.layer.cornerRadius = 2;
}
- (void)setDish:(Dish *)dish
{
    _priceView.backgroundColor = [Utils appRedColor];
    _priceView.layer.cornerRadius = 2;
    _dish = dish;
    _titleLabel.attributedText = [MenuCell textForDish:dish];
    _priceLabel.text = [NSString stringWithFormat: @"₪ %@", dish.dishPrice];
}

+ (NSAttributedString *)textForDish:(Dish*)dish
{
    NSString *baseStr = [NSString stringWithFormat:@"%@\n%@",dish.dishName, dish.dishDesc];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:baseStr attributes:nil];
    [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:16] range:[baseStr rangeOfString:dish.dishName]];
    
    return str;
}


+ (CGFloat)heightForDish:(Dish*)dish
{
    NSMutableParagraphStyle *hebrewParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    [hebrewParagraphStyle setAlignment:NSTextAlignmentRight];
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:dish.dishName attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:16], NSParagraphStyleAttributeName : hebrewParagraphStyle}];
    if([[UIDevice currentDevice] systemVersion].floatValue >= 7.0f)
    {
        [title addAttribute:NSWritingDirectionAttributeName value:@[@(NSWritingDirectionRightToLeft | NSTextWritingDirectionOverride)] range:NSMakeRange(0, dish.dishName.length)];
    }

    NSMutableAttributedString *descr = [[NSMutableAttributedString alloc] initWithString:dish.dishDesc attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16], NSParagraphStyleAttributeName: hebrewParagraphStyle}];
    
    if([[UIDevice currentDevice] systemVersion].floatValue >= 7.0f)
    {
        [descr addAttribute:NSWritingDirectionAttributeName value:@[@(NSWritingDirectionRightToLeft | NSTextWritingDirectionOverride)] range:NSMakeRange(0, dish.dishDesc.length)];
    }
    
    CGSize nameSize = [title boundingRectWithSize: CGSizeMake(210, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
      CGSize descrSize = [descr boundingRectWithSize: CGSizeMake(210, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
    CGFloat cellHeight = ceilf(nameSize.height + descrSize.height + 10);
    return MAX(cellHeight, 52.0f);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if(!animated)
    {
        if(selected)
        {
            _selectView.alpha = 1;
        }
        else
        {
            _selectView.alpha = 0;
        }
    }
    else
    {
        [UIView animateWithDuration:0.2 animations:^{
            if(selected)
            {
                _selectView.alpha = 1;
            }
            else
            {
                _selectView.alpha = 0;
            }
        }];
    }
}

@end
