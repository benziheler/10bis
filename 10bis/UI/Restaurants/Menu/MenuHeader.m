//
//  MenuHeader.m
//  10bis
//
//  Created by Vadim Pavlov on 24.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "MenuHeader.h"
#import <CoreText/CoreText.h>
#import "FrameAccessor.h"

@implementation MenuHeader
{
    __weak IBOutlet UIImageView *_bgView;
    __weak IBOutlet UIImageView *_arrowView;
}
- (void)awakeFromNib
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(click:)];
    [self addGestureRecognizer: tap];
}
- (void)setIsOpen:(BOOL)isOpen
{
    _isOpen = isOpen;
    UIImage *arrowImage = [UIImage imageNamed: isOpen ? @"icon_next_up" : @"icon_next_down"];
    _arrowView.image = arrowImage;
}
- (void)click:(id)sender
{
    [UIView animateWithDuration: 0.2f
                     animations:^{
                         _arrowView.transform = CGAffineTransformMakeRotation(M_PI);
                     } completion:^(BOOL finished) {
                         _arrowView.transform = CGAffineTransformIdentity;
                         [self setIsOpen: !_isOpen];
                     }];
    
    _didClickOnHeader(self);
}
+ (CGFloat)heightForText:(NSString*)headerText
{
    NSMutableParagraphStyle *hebrewParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    [hebrewParagraphStyle setAlignment:NSTextAlignmentRight];
    NSMutableDictionary *attr = [@{NSFontAttributeName: [UIFont systemFontOfSize:20],  NSParagraphStyleAttributeName: hebrewParagraphStyle} mutableCopy];
    if([[UIDevice currentDevice] systemVersion].floatValue >= 7.0f)
    {
        [attr setObject:@[@(NSWritingDirectionRightToLeft | NSTextWritingDirectionOverride)] forKey:NSWritingDirectionAttributeName];
    }
    
    NSAttributedString *descr = [[NSAttributedString alloc] initWithString:headerText attributes:attr];
    return [MenuHeader heightForAttrString:descr];
}

+ (CGFloat)heightForAttrString:(NSAttributedString *)attrString
{
    CGFloat totalHeight = 0;
    NSArray *lines = [attrString.string componentsSeparatedByString:@"\n"];
    for (NSString *str in lines)
    {
        NSMutableParagraphStyle *hebrewParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        [hebrewParagraphStyle setAlignment:NSTextAlignmentRight];
        UIFont *font = [UIFont fontWithName: @"Arial" size: 20];
        if([lines indexOfObject:str] > 0)
        {
            font = [UIFont fontWithName: @"Arial" size: 15];
        }
        
        NSMutableDictionary *attr = [@{NSFontAttributeName: font, NSParagraphStyleAttributeName: hebrewParagraphStyle} mutableCopy];
        if([[UIDevice currentDevice] systemVersion].floatValue >= 7.0f)
        {
            [attr setObject:@[@(NSWritingDirectionRightToLeft | NSTextWritingDirectionOverride)] forKey:NSWritingDirectionAttributeName];
        }
        

        NSAttributedString *attributed = [[NSAttributedString alloc] initWithString:str attributes:attr];

        CGSize size = [attributed boundingRectWithSize: CGSizeMake(230, CGFLOAT_MAX) options: NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context: nil].size;
        totalHeight += size.height;
        
    }
    
    CGFloat cellHeight = ceilf(totalHeight + 10);
    return MAX(cellHeight, 52.0f);
}

- (void)setHeaderHeight:(CGFloat)headerHeight
{
    self.height = headerHeight;
    _headerHeight = headerHeight;
    _headerLabel.backgroundColor = [UIColor clearColor];
    
    
    CGRect bgRect = _bgView.frame;
    bgRect.size.height = headerHeight-4;
    _bgView.frame = bgRect;
    
    [UIView animateWithDuration: 0.2f animations:^{
        CGRect arrowRect = _arrowView.frame;
        arrowRect.origin.y = roundf((headerHeight - arrowRect.size.height)/2);
        _arrowView.frame = arrowRect;
    }];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect frame = _bgView.frame;
    frame.origin.y = 2;
    frame.size.height = self.frame.size.height - 2;
}

@end
