//
//  FilterView.m
//  10bis
//
//  Created by Anton Vilimets on 5/22/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "FilterView.h"
#import "FilterCell.h"
#import "UIControl+BBlock.h"
#import "FrameAccessor.h"


@implementation FilterView
{
    NSArray *headers;
    NSArray *cuisinesList;
    NSArray *pickupFilters;
    NSArray *deliveryFilters;
    
    NSArray *filterTitles;
    NSInteger _selectedDeliverySortFilter;
    NSInteger _selectedPickupSortFilter;
    
    NSInteger _selectedCuisineFilter;
    BOOL discoundEnabled;
    BOOL kosherEnabled;
    NSMutableArray *footers;
    
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (id)filterView
{
    NSArray *xib = [[NSBundle mainBundle] loadNibNamed: NSStringFromClass(self.class) owner: self options: nil];
    return xib[0];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    footers = [NSMutableArray array];
    for(int i=0;i<3;i++)
    {
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, self.contentView.frame.size.width - 10, 2)];
        lbl.backgroundColor = RGB(243, 211, 212);
        lbl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, 22)];
        view.backgroundColor = [UIColor whiteColor];
        [view addSubview:lbl];
        [footers addObject:view];
    }
    self.isSortingEnabled = YES;
    DeliveryMethod method = [DataManager deliveryMethod];
    _selectedDeliverySortFilter = 0;
    _selectedPickupSortFilter = 0;
    _selectedCuisineFilter = 0;
    if(method == DeliveryMethodNone)
    {
        headers = @[_topHeaderView,_deliverySortHeaderView, _pickupSortHeaderView];
        _pickupSortHeaderView.hidden = NO;
        _deliverySortHeaderView.hidden = NO;
        
    }
    else if(method == DeliveryMethodDelivery)
    {
        headers = @[_topHeaderView,_deliverySortHeaderView];
        _deliverySortHeaderView.hidden = NO;
    }
    else
    {
        headers = @[_topHeaderView,_pickupSortHeaderView];
        _pickupSortHeaderView.hidden = NO;
    }
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    openedHeader = -1;
    NSMutableArray *cuisines = [[[NSUserDefaults standardUserDefaults] arrayForKey:kCuisinesFilterList] mutableCopy];
    [cuisines insertObject:@{@"CuisineTypeName" : @"כל סוגי המסעדות"} atIndex:0];
    cuisinesList = cuisines;
    
    deliveryFilters = @[@{@"name": @"ערך קופון", @"field": @"discountCouponPercent", @"icon" : @"icn_fs_coupon"},
                        @{@"name": @"מחיר משלוח", @"field": @"deliveryPriceForOrder", @"icon" : @"icn_fs_delivery"},
                        @{@"name": @"מינימום הזמנה", @"field": @"minimumPriceForOrder", @"icon" : @"icn_fs_order"},
                        @{@"name": @"שם המסעדה", @"field": @"name", @"icon" : @"icn_fs_rest_name"},
                        @{@"name": @"דירוג", @"field": @"reviewsRank", @"icon" : @"icn_fs_rate"}];
    
    pickupFilters = @[@{@"name": @"מרחק", @"field": @"distanceFromUser", @"icon" : @"icn_fs_distance"},
                      @{@"name": @"ערך קופון", @"field": @"discountCouponPercent", @"icon" : @"icn_fs_coupon"},
                      @{@"name": @"דירוג", @"field": @"reviewsRank", @"icon" : @"icn_fs_rate"},
                      @{@"name": @"שם המסעדה", @"field": @"name", @"icon" : @"icn_fs_rest_name"}];
    
    
    
    
    filterTitles = @[@"כולל הנחה", @"מסעדות כשרות",];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FilterCell" bundle:nil] forCellReuseIdentifier:@"FilterCell"];
    
    [_tableView reloadData];
    [self updateHeaders];

}

- (void)updateHeaders
{
    NSDictionary *cuisineDict = cuisinesList[_selectedCuisineFilter];
    [_cuisineHaderButton setTitle:cuisineDict[@"CuisineTypeName"] forState:UIControlStateNormal];
    [_deliveryHeaderButton setTitle:[deliveryFilters[_selectedDeliverySortFilter] objectForKey:@"name"] forState:UIControlStateNormal];
    [_pickupHeaderButton setTitle:[pickupFilters[_selectedPickupSortFilter] objectForKey:@"name"] forState:UIControlStateNormal];
    UIImage *pickupIcon = [UIImage imageNamed:[pickupFilters[_selectedPickupSortFilter] objectForKey:@"icon"]];
    UIImage *deliveryIcon = [UIImage imageNamed:[deliveryFilters[_selectedDeliverySortFilter] objectForKey:@"icon"]];
    _deliveryHeaderImageVew.image = deliveryIcon;
    _pickupHeaderImageView.image = pickupIcon;
}

- (void)setupWithDict:(NSDictionary *)dict
{
    _selectedDeliverySortFilter = 0;
    _selectedPickupSortFilter = 0;
    _selectedCuisineFilter = 0;
    if (dict[@"cuisine"]) {
        _selectedCuisineFilter = [cuisinesList indexOfObject:dict[@"cuisine"]];
        if(_selectedCuisineFilter < 0 || _selectedCuisineFilter >= cuisinesList.count)
        {
            _selectedCuisineFilter = 0;
        }
    }
    
    if(dict[@"sortByDelivery"])
    {
        _selectedDeliverySortFilter = [deliveryFilters indexOfObject:dict[@"sortByDelivery"]];
        if(_selectedDeliverySortFilter >= deliveryFilters.count)    _selectedDeliverySortFilter = 0;
    }
    if(dict[@"sortByPickup"])
    {
        _selectedPickupSortFilter = [pickupFilters indexOfObject:dict[@"sortByPickup"]];
        if(_selectedPickupSortFilter >= pickupFilters.count)    _selectedPickupSortFilter = 0;
    }
    kosherEnabled = [dict[kKosherKey] boolValue];
    discoundEnabled = [dict[kHasDiscountKey] boolValue];
    [_tableView reloadData];
    [self updateHeaders];
}

- (IBAction)headerPerssed:(UIButton *)sender
{
    NSInteger section1 = [self tableView:_tableView numberOfRowsInSection:1];
    NSMutableArray *indexToRemove = [NSMutableArray array];
    NSMutableArray *indexToAdd = [NSMutableArray array];
    
    if(sender.tag == openedHeader)
    {
        NSMutableIndexSet *sectionsToAdd = [NSMutableIndexSet indexSet];
        _topHeaderArrow.highlighted = NO;
        _deliveryHeaderArrow.highlighted = NO;
        if(sender.tag == 0)
        {
            [_tableView setContentOffset:CGPointMake(0, 0) animated:NO];
            openedHeader = -1;
            [_tableView reloadData];
            return;
        }
        else if(sender.tag == 1)
        {
            [sectionsToAdd addIndex:0];
            if([DataManager deliveryMethod] == DeliveryMethodNone)
            {
                [sectionsToAdd addIndex:2];
            }
            for(int i=0;i<section1;i++)
            {
                [indexToRemove addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        else if (sender.tag == 2)
        {
            [sectionsToAdd addIndex:0];
            if([DataManager deliveryMethod] == DeliveryMethodNone)
            {
                [sectionsToAdd addIndex:1];
            }
            for(int i=0;i<section1;i++)
            {
                [indexToRemove addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        openedHeader = -1;
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexToRemove withRowAnimation:UITableViewRowAnimationTop];
        [_tableView insertRowsAtIndexPaths:indexToAdd withRowAnimation:UITableViewRowAnimationTop];
        UITableViewRowAnimation anim = UITableViewRowAnimationTop;
        
        [_tableView insertSections:sectionsToAdd withRowAnimation:anim];
        
        [_tableView endUpdates];
        
        
    }
    else
    {
        NSMutableIndexSet *sectionsToRemove = [NSMutableIndexSet indexSet];
        if(sender.tag == 0)
        {
            if(self.isSortingEnabled)
            {
            [sectionsToRemove addIndex:1];
            }
            if([DataManager deliveryMethod] == DeliveryMethodNone)
            {
                [sectionsToRemove addIndex:2];
            }
            _topHeaderArrow.highlighted = YES;
            _deliveryHeaderArrow.highlighted = NO;
            _pickupHeaderArrow.highlighted = NO;
            NSInteger numAfter = cuisinesList.count;
            
            numAfter+= filterTitles.count;
            for(int i=0;i<filterTitles.count;i++)
            {
                [indexToRemove addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            for(int i=0;i<cuisinesList.count;i++)
            {
                [indexToAdd addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        else if(sender.tag == 1)
        {
            [sectionsToRemove addIndex:0];
            if([DataManager deliveryMethod] == DeliveryMethodNone)
            {
                [sectionsToRemove addIndex:2];
            }
            _topHeaderArrow.highlighted = NO;
            _pickupHeaderArrow.highlighted = NO;
            _deliveryHeaderArrow.highlighted = YES;
            for(int i=0;i<deliveryFilters.count;i++)
            {
                [indexToAdd addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        else if(sender.tag == 2)
        {
            [sectionsToRemove addIndex:0];
            if([DataManager deliveryMethod] == DeliveryMethodNone)
            {
                [sectionsToRemove addIndex:1];
            }
            _topHeaderArrow.highlighted = NO;
            _pickupHeaderArrow.highlighted = YES;
            _deliveryHeaderArrow.highlighted = NO;
            for(int i=0;i<pickupFilters.count;i++)
            {
                [indexToAdd addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
        }
        openedHeader = sender.tag;
        
        if([DataManager deliveryMethod] == DeliveryMethodNone && openedHeader == 2)
        {
            [_tableView reloadData];
        }
        else
        {
            [_tableView beginUpdates];
            UITableViewRowAnimation anim = UITableViewRowAnimationTop;
            
            [_tableView deleteSections:sectionsToRemove withRowAnimation:anim];
            [_tableView deleteRowsAtIndexPaths:indexToRemove withRowAnimation:UITableViewRowAnimationTop];
            [_tableView insertRowsAtIndexPaths:indexToAdd withRowAnimation:UITableViewRowAnimationTop];
            
            [_tableView endUpdates];
        }
    }
    
    
}

- (IBAction)donePressed:(id)sender
{
    NSDictionary *result = [self buildResult];
    if(_block)
    {
        _block(result);
    }
    NSMutableDictionary *savedDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
    if(!savedDict)
    {
        savedDict = [NSMutableDictionary dictionary];
    }
    else
    {
        savedDict = [savedDict mutableCopy];
    }
    [savedDict setObject:[NSNumber numberWithBool:kosherEnabled] forKey:kKosherKey];
    [[NSUserDefaults standardUserDefaults] setObject:savedDict forKey:[DataManager filterSettingKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];


    [self hide];
    
}

- (NSDictionary *)buildResult
{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    if(_selectedDeliverySortFilter >= 0)
    {
        [result setObject:deliveryFilters[_selectedDeliverySortFilter] forKey:@"sortByDelivery"];
    }
    if(_selectedPickupSortFilter >= 0)
    {
        [result setObject:pickupFilters[_selectedPickupSortFilter] forKey:@"sortByPickup"];
    }
    if(_selectedCuisineFilter > 0)
    {
        [result setObject:cuisinesList[_selectedCuisineFilter] forKey:@"cuisine"];
    }
    [result setObject:[NSNumber numberWithBool:discoundEnabled] forKey:kHasDiscountKey];
    [result setObject:[NSNumber numberWithBool:kosherEnabled] forKey:kKosherKey];
   /* if(_selectedPickupSortFilter == 0 && _selectedDeliverySortFilter && _selectedCuisineFilter == 0 && !discoundEnabled && !kosherEnabled)
    {
        return nil;
    }
    else
    {
        return result;
    }*/
    return result;
}

- (IBAction)closePressed:(id)sender
{
//    discoundEnabled = NO;
//    kosherEnabled = NO;
//    _selectedCuisineFilter = 0;
//    _selectedDeliverySortFilter = 0;
//    _selectedPickupSortFilter = 0;
    if(_block)
    {
        _block(nil);
    }
    if(self.closeButton.hidden == NO)
    {
        [self hide];
    }
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(openedHeader == -1 )
    {
        if([DataManager deliveryMethod] == DeliveryMethodNone)
        {
            return 3;
        }
        else if(self.isSortingEnabled)
        {
            return 2;
        }
    }
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(openedHeader == -1)
    {
        if(section == 0)
        {
            return filterTitles.count;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if(openedHeader == 0)
        {
            return cuisinesList.count;
        }
        else if(openedHeader == 1)
        {
            return deliveryFilters.count;
        }
        else if(openedHeader == 2)
        {
            return pickupFilters.count;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterCell *cell = (FilterCell *)[tableView dequeueReusableCellWithIdentifier:@"FilterCell" forIndexPath:indexPath];
    [cell.radioButton addActionForControlEvents:UIControlEventTouchUpInside withBlock:^(UIButton *control, UIEvent *event) {
    }];
    
    [cell.switchView addActionForControlEvents:UIControlEventValueChanged withBlock:^(UISwitch *control, UIEvent *event) {
        NSInteger index = indexPath.row;
        switch (index) {
            case 0:
                discoundEnabled = control.isOn;
                break;
            case 1:
                kosherEnabled = control.isOn;
                break;
                
            default:
                break;
        }
    }];
    
    
    if(openedHeader == -1)
    {
        NSInteger index = indexPath.row;
        BOOL selected = NO;
        UIImage *icon = nil;
        if(index == 0)
        {
            selected = discoundEnabled;
            icon = [UIImage imageNamed:@"icn_fs_discount"];
        }
        else if(index == 1)
        {
            selected = kosherEnabled;
            icon = [UIImage imageNamed:@"icn_fs_kosher"];
        }
        [cell setupWithType:CellTypeSwitch title:filterTitles[indexPath.row] icon:icon selected:selected];
    }
    else if(openedHeader == 0)
    {
        if(indexPath.row < cuisinesList.count)
        {
            BOOL selected = (indexPath.row == _selectedCuisineFilter);
            NSDictionary *cuisineDict = cuisinesList[indexPath.row];
            [cell setupWithType:CellTypeRadioButton title:cuisineDict[@"CuisineTypeName"] icon:nil selected:selected];
        }
        /* else
         {
         NSInteger index = indexPath.row - cuisinesList.count;
         BOOL selected = NO;
         if(index == 0) selected = discoundEnabled;
         else if(index == 1) selected = kosherEnabled;
         [cell setupWithType:CellTypeSwitch title:filterTitles[indexPath.row-cuisinesList.count] selected:selected];
         
         }*/
    }
    else
    {
        if([DataManager deliveryMethod] == DeliveryMethodDelivery || ([DataManager deliveryMethod] == DeliveryMethodNone && openedHeader == 1))
        {
            BOOL selected = (indexPath.row == _selectedDeliverySortFilter);
            UIImage *icon = [UIImage imageNamed:[deliveryFilters[indexPath.row] objectForKey:@"icon"]];
            [cell setupWithType:CellTypeImage title:[deliveryFilters[indexPath.row] objectForKey:@"name"] icon:icon selected:selected];
        }
        else
        {
            BOOL selected = (indexPath.row == _selectedPickupSortFilter);
            UIImage *icon = [UIImage imageNamed:[pickupFilters[indexPath.row] objectForKey:@"icon"]];

            [cell setupWithType:CellTypeImage title:[pickupFilters[indexPath.row] objectForKey:@"name"] icon:icon selected:selected];
        }
    }
    if(openedHeader >= 0)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    else
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIButton *targetButton = nil;
    if(openedHeader == 0)
    {
        FilterCell *selectedCell = (FilterCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedCuisineFilter inSection:0]];

        selectedCell.radioButton.selected = NO;
        FilterCell *cell = (FilterCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.radioButton.selected = YES;
            _selectedCuisineFilter = indexPath.row;
            NSDictionary *cuisineDict = cuisinesList[_selectedCuisineFilter];
            [_cuisineHaderButton setTitle:cuisineDict[@"CuisineTypeName"] forState:UIControlStateNormal];
            //  openedHeader = -1;
        
            targetButton = _cuisineHaderButton;
    }
    else if(openedHeader == 1)
    {
        _selectedDeliverySortFilter = indexPath.row;
        [_deliveryHeaderButton setTitle:[deliveryFilters[_selectedDeliverySortFilter]objectForKey:@"name"] forState:UIControlStateNormal];
        UIImage *icon = [UIImage imageNamed:[deliveryFilters[indexPath.row] objectForKey:@"icon"]];
        _deliveryHeaderImageVew.image = icon;
        targetButton = _deliveryHeaderButton;
    }
    
    else if(openedHeader == 2)
    {
        _selectedPickupSortFilter = indexPath.row;
        [_pickupHeaderButton setTitle:[pickupFilters[_selectedPickupSortFilter]objectForKey:@"name"] forState:UIControlStateNormal];
        UIImage *icon = [UIImage imageNamed:[pickupFilters[indexPath.row] objectForKey:@"icon"]];
        _pickupHeaderImageView.image = icon;
        targetButton = _pickupHeaderButton;
    }
    if(targetButton)
    {
        double delayInSeconds = 0.2;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self headerPerssed:targetButton];

            });
    }

  /*  if(indexPath.section == 0)
    {
   
    }
    else if(indexPath.section == 1)
    {
        if([DataManager deliveryMethod] == DeliveryMethodDelivery || ([DataManager deliveryMethod] == DeliveryMethodNone && indexPath.section == 1))
        {
            _selectedDeliverySortFilter = indexPath.row;
        }
        else
        {
            _selectedPickupSortFilter = indexPath.row;
            
        }
    }*/
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [headers objectAtIndex:section];
    if(openedHeader >= 0)
    {
        if(openedHeader == 2 && headers.count == 2)
        {
            header = [headers objectAtIndex:1];
            
        }
        else
        {
            header = [headers objectAtIndex:openedHeader];
        }
    }

    header.hidden = NO;
    header.width = self.tableView.frame.size.width;
    _deliveryHeaderButton.width = header.width - 20;
    _pickupHeaderButton.width = header.width - 20;
    _cuisineHaderButton.width = header.width - 20;
    
    return header;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
    return footers[section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(openedHeader <= 0 && section == 0)
    {
        return 93;
    }
    return 104;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)showWithCompletion:(void (^)(NSDictionary *type))block

{
    [_tableView reloadData];
    self.block = block;
    self.alpha = 0;
    self.hidden = NO;
    _contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.0f, 0.0f);
    _contentView.hidden = YES;
    [UIView animateWithDuration:0.15 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        _contentView.hidden = NO;
        
        [UIView animateWithDuration:0.2 animations:^{
            _contentView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
        }];
        
    }];
    
}

- (void)hide
{
    [UIView animateWithDuration:0.2 animations:^{
        
        _contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.0f, 0.0f);
        
    } completion:^(BOOL finished) {
        _contentView.hidden = YES;
        [UIView animateWithDuration:0.1 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
            self.alpha = 1;
        }];
    }];
}

@end
