//
//  FilterCell.m
//  10bis
//
//  Created by Anton Vilimets on 5/22/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell
{
    FilterCellType _type;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    _switchView.onTintColor = [Utils appRedColor];
}

- (void)setupWithType:(FilterCellType)type title:(NSString *)title icon:(UIImage *)icon selected:(BOOL)selected
{
    _type = type;
    CGRect frame = _titleLabel.frame;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _iconImageView.image = icon;
    if(type == CellTypeRadioButton)
    {
        _titleLabel.textAlignment = NSTextAlignmentRight;
        _switchView.hidden = YES;
        _radioButton.hidden = NO;
        _radioButton.selected = selected;
        _iconImageView.hidden = YES;
    }
    else if(type == CellTypeSwitch)
    {
        _titleLabel.textAlignment = NSTextAlignmentRight;
        _switchView.hidden = NO;
        _radioButton.hidden = YES;
        [_switchView setOn:selected];
        _iconImageView.hidden = NO;

    }
    else if (type == CellTypeImage)
    {
        _switchView.hidden = YES;
        _radioButton.hidden = YES;
        _iconImageView.hidden = NO;
    }
    _titleLabel.text = title;
    _titleLabel.frame = frame;
    [self setSelected:selected];
}

- (BOOL)isSelected
{
    if(_type == CellTypeRadioButton)
    {
        return _radioButton.isSelected;
    }
    else if (_type == CellTypeSwitch)
    {
        return _switchView.isOn;
    }
    return NO;
}

- (void)setSelected:(BOOL)selected
{
 //   [super setSelected:selected animated:animated];
 //   [_switchView setOn:selected];
}

@end
