//
//  SubChoiceCell.h
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Choice, SubChoice;
@interface ChoiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) Choice *choice;
@property (nonatomic, strong) SubChoice *subChoice;

+ (CGFloat)heightForSubChoice:(SubChoice*)subChoice;
@end
