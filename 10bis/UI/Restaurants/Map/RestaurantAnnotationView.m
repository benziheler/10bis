//
//  RestaurantAnnotationView.m
//  10bis
//
//  Created by Vadim Pavlov on 29.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "RestaurantAnnotationView.h"
#import "StarsView.h"
#import "RestaurantAnnotation.h"
#import "Restaurant.h"
#import "RestaurantCluster.h"
#import "NSArray+Sort.h"
#import "CustomAlert.h"
#import "RestAnnotationCell.h"
@implementation RestaurantAnnotationView
{


    __weak IBOutlet UILabel *_numberLabel;
    __weak IBOutlet UIPageControl *_pageControl;
    
    NSInteger _currentPage;
    CGFloat _oldOffset;
    BOOL _isAnimating;

    NSMutableDictionary *_updatingDict;
}
+ (id)annotationView
{
    NSArray *xib = [[NSBundle mainBundle] loadNibNamed: NSStringFromClass(self.class) owner: self options: nil];
    return xib[0];
}
- (void)awakeFromNib
{
    [_collectionView registerNib:[UINib nibWithNibName:@"RestAnnotationCell" bundle:nil] forCellWithReuseIdentifier:@"RestAnnotationCell"];
    _updatingDict = [NSMutableDictionary dictionaryWithCapacity: 3];
    }

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)setAnnotation:(RestaurantAnnotation *)annotation
{
    _annotation = annotation;
    _currentPage = 0;
    _oldOffset = 0.0f;
    
    if (annotation.restaurant)
    {
        _numberLabel.hidden = YES;
        _pageControl.numberOfPages = 0;
    }
    else if (annotation.cluster)
    {
        _numberLabel.hidden = NO;
        [_collectionView setContentOffset:CGPointMake(0, 0)];
        [_pageControl setCurrentPage:0];
        [self updateNumber];
    }
    [_collectionView reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(_annotation.cluster)
    {
        return _annotation.cluster.restaurants.count;
    }
    else
    {
        return 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RestAnnotationCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RestAnnotationCell" forIndexPath:indexPath];
    [cell.tapGesture addTarget:self action:@selector(selectedRestaurant)];
    Restaurant *rest = nil;
    if(_annotation.cluster)
    {
        rest = _annotation.cluster.restaurants[indexPath.item];
    }
    else
    {
        rest = _annotation.restaurant;
    }
    [cell setRestaurant:rest];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)selectedRestaurant
{
    Restaurant *rest = nil;
    if(_annotation.cluster)
    {
        rest = _annotation.cluster.restaurants[_currentPage];
    }
    else
    {
        rest = _annotation.restaurant;
    }
    self.didselectRestaurant(rest);
}

- (void)updateNumber
{
    NSInteger numberOfRestaurans = _annotation.cluster.restaurants.count;
    _currentPage = _collectionView.contentOffset.x / 140;
    _numberLabel.text = [NSString stringWithFormat: @"%ld/%ld", _currentPage + 1, (long)numberOfRestaurans];
//    NSInteger controlPage = _currentPage % 5;
    
   // NSInteger bigPageOffset = (_currentPage / 5) * 5;
   // _pageControl.numberOfPages = MIN(numberOfRestaurans - bigPageOffset, 5);
    _pageControl.numberOfPages = numberOfRestaurans;
    _pageControl.currentPage = _currentPage;//controlPage;
    _annotation.clusterSelectedIndex = _currentPage;
}
#pragma mark - Scroll View
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"Did End Decelerating");
    [self updateScrollView];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == NO)
        [self updateScrollView];
}
- (void)updateScrollView
{
    [self updateNumber];
}


@end
