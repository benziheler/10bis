//
//  RestAnnotationCell.h
//  10bis
//
//  Created by Anton Vilimets on 6/27/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarsView.h"
#import "Restaurant.h"

@interface RestAnnotationCell : UICollectionViewCell <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet StarsView *starView;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *ribbonView;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UIView *kosherIcon;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@property (weak, nonatomic) IBOutlet UIImageView *leftIcon;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (weak, nonatomic) IBOutlet UILabel *deliveryPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeDeliveryLabel;

@property (weak, nonatomic) IBOutlet UILabel *hoursLAbel;
@property (strong, nonatomic) Restaurant *restaurant;
@property (weak, nonatomic) IBOutlet UIView *pickupDetails;
@property (weak, nonatomic) IBOutlet UIView *deliveryDetails;

@end
