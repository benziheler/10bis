//
//  RestAnnotationCell.m
//  10bis
//
//  Created by Anton Vilimets on 6/27/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "RestAnnotationCell.h"
#import "CustomAlert.h"

@implementation RestAnnotationCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.tapGesture = [[UITapGestureRecognizer alloc] init];
    self.tapGesture.cancelsTouchesInView = YES;
    self.tapGesture.delegate = self;
    [self addGestureRecognizer:self.tapGesture];
    
    [_categoryButton setValue: @(UIBaselineAdjustmentAlignCenters) forKeyPath: @"titleLabel.baselineAdjustment"];
    
    [_logoView setValue: @(2.0f) forKeyPath: @"layer.cornerRadius"];
    [_logoView setValue: @(YES) forKeyPath: @"layer.masksToBounds"];
    
    [_discountLabel setValue: [NSValue valueWithCGAffineTransform: CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-30))] forKey: @"transform"];

    
    UIImage *bgImage = [_categoryButton backgroundImageForState: UIControlStateNormal];
    UIImage *strBgImage = [bgImage stretchableImageWithLeftCapWidth: bgImage.size.width/2 topCapHeight:bgImage.size.height/2];

        [_categoryButton setBackgroundImage: strBgImage forState: UIControlStateNormal];

}
- (IBAction)btnTap:(id)sender {
}

- (void)setRestaurant:(Restaurant *)restaurant
{
    _restaurant = restaurant;
    [self updateToRestaurant:restaurant index:0];
}

- (void)updateToRestaurant:(Restaurant*)restaurant index:(NSInteger)index
{
    _starView.isRTL = YES;
    
  //  _updatingDict[@(index)] = restaurant.restaurantId;
    _leftIcon.hidden = NO;
    _rightIcon.hidden = NO;
    CLLocationCoordinate2D curLocation = [[DataManager sharedManager] currentLocation].coordinate;
    if (curLocation.longitude != 0 && curLocation.longitude != 0)
    {
        CLLocation *restaurantLocation = [[CLLocation alloc] initWithLatitude: _restaurant.latitude longitude: _restaurant.longitude];
        CLLocation *userLocation = [[CLLocation alloc] initWithLatitude: curLocation.latitude longitude: curLocation.longitude];
        
        CLLocationDistance distance = [userLocation distanceFromLocation: restaurantLocation] / 1000;
        _locationLabel.text = [NSString stringWithFormat: @"%0.2f %@", distance, @"ק״מ ממך"];
    }
    else
    {
        _locationLabel.text = [NSString stringWithFormat: @"%0.2f %@", 0.0f, @"ק״מ ממך"];
    }


    
    _nameLabel.text = restaurant.name;
    _kosherIcon.hidden = ![restaurant.isKosher isEqualToString: @"Kosher"];
    
    NSMutableArray *address = [NSMutableArray arrayWithCapacity: 2];
    //    if (restaurant.cityName.length)
    //        [address addObject: restaurant.cityName];
    if (restaurant.street.length)
        [address addObject: restaurant.street];
    _addressLabel.text = [address componentsJoinedByString: @", "];
    _phoneLabel.text = restaurant.phone;
    _deliveryPhoneLabel.text = restaurant.phone;
    _phoneLabel.hidden = NO;
    
    [_starView setStars: restaurant.reviewsRank];
    _starView.userInteractionEnabled = NO;

    
    NSString *reviewsText = [NSString stringWithFormat: @"%ld המלצות", (long)restaurant.numberOfReviews];
    NSMutableAttributedString *attrReviewsText = [[NSMutableAttributedString alloc] initWithString: reviewsText];
    [Utils addBoldAttributeToString: attrReviewsText forText: [NSString stringWithFormat: @"%ld", (long)restaurant.numberOfReviews] font: 10];
    _ratingLabel.attributedText = attrReviewsText;
    
    
    // category
    [_categoryButton setTitle: restaurant.cuisineList forState: UIControlStateNormal];
    _categoryButton.hidden = restaurant.cuisineList.length == 0;
    
    _logoView.image = nil;
    
    [Utils loadImage: restaurant.logoURL completion:^(UIImage *image) {
                   _logoView.image = image;
           }];
    
    BOOL couponHidden = NO;
    NSString *couponText = nil;
    DeliveryMethod method = [DataManager deliveryMethod];
    switch (method) {
        case DeliveryMethodDelivery:
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
            break;
        case DeliveryMethodPickup:
            couponHidden = restaurant.discountCouponPercent == 0;
            couponText = [NSString stringWithFormat: @"%ld%%", (long)restaurant.discountCouponPercent];
            break;
        case DeliveryMethodSitting:
            if (restaurant.isHappyHourActive == NO ||
                [restaurant.happyHourDiscount isEqualToString: @"0%"])
            {
                couponHidden = YES;
            }
            couponText = restaurant.happyHourDiscount;
            break;
        default:
            break;
    }
    
    // discount
    _discountLabel.hidden = _ribbonView.hidden = couponHidden;
    if ((method == DeliveryMethodSitting || restaurant.couponHasRestrictions) && couponText)
        couponText = [@"*" stringByAppendingString: couponText];
    
    _discountLabel.text = couponText;
    _deliveryDetails.hidden = YES;
    _pickupDetails.hidden = NO;
    // hours or delivery fee
    if (method == DeliveryMethodDelivery)
    {
        _deliveryDetails.hidden = NO;
        _pickupDetails.hidden = YES;
      
        NSString *minOrder = nil;
        NSString *deliveryPrice = nil;
        if (restaurant.minimumPriceForOrder == 0)
        {
            minOrder = @"אין מינימום להזמנה";
        }
        else
        {
            minOrder = [NSString stringWithFormat: @"מינימום הזמנה: %ld ₪" , (long)restaurant.minimumPriceForOrder];
        }
        
        if (restaurant.deliveryPriceForOrder == 0)
        {
            deliveryPrice = @"משלוח חינם";
        }
        else
        {
            deliveryPrice = [NSString stringWithFormat: @"דמי משלוח: %ld ₪", (long)restaurant.deliveryPriceForOrder];
        }
        NSString *orderString = [NSString stringWithFormat: @"%@ | הזמנת מינימום %@", _restaurant.poolSum, minOrder];
        if(_restaurant.isOverPoolMin)
        {
            orderString = @"ההזמנה עומדת בדרישת המינימום";
        }
        NSMutableAttributedString *attrOrder = [[NSMutableAttributedString alloc] initWithString: orderString];
        
        NSString *timeString = [NSString stringWithFormat: @"%@ | הזמנה עד %@", deliveryPrice, _restaurant.deliveryEndTime];
        NSMutableAttributedString *attrTime = [[NSMutableAttributedString alloc] initWithString: timeString];
        
        [Utils addBoldAttributeToString:attrOrder forText: orderString font: _locationLabel.font.pointSize];
        [Utils addBoldAttributeToString: attrTime forText: timeString font: _locationLabel.font.pointSize];
        _sumDeliveryLabel.attributedText = attrOrder;
        _timeDeliveryLabel.attributedText = attrTime;
        if(_restaurant.companyFlag == NO)
        {
            _sumDeliveryLabel.attributedText = attrTime;
            _timeDeliveryLabel.attributedText = nil;
            _timeDeliveryLabel.text = nil;
        }
    }
    else if(method == DeliveryMethodPickup)
    {
        _locationIcon.hidden = NO;
        _locationLabel.hidden = NO;
        NSString *hoursText = [NSString stringWithFormat: @"%@", restaurant.pickupActivityHours];
        NSMutableAttributedString *attrHoursText = [[NSMutableAttributedString alloc] initWithString: hoursText];
        [Utils addBoldAttributeToString: attrHoursText forText: restaurant.pickupActivityHours font: _locationLabel.font.pointSize];
        _hoursLAbel.attributedText = attrHoursText;
    }
    else if (method == DeliveryMethodSitting)
    {
        _phoneLabel.hidden = YES;
        _hoursLAbel.text = _phoneLabel.text;
        _rightIcon.image = [UIImage imageNamed:@"icn_phone_white"];
        _leftIcon.hidden = YES;
        _locationIcon.hidden = NO;
        _locationLabel.hidden = NO;
    }
    
    
  /*  if (couponHidden)
    {
        CGRect nameRect = _nameRect;
        nameRect.origin.x = 5.0f;
        nameRect.size.width = CGRectGetMaxX(_nameRect) - nameRect.origin.x;
        CGRect addressRect = _addressRect;
        addressRect.origin.x = nameRect.origin.x;
        addressRect.size.width = nameRect.size.width;
        nameLabel.frame = nameRect;
        addressLabel.frame = addressRect;
    }
    else
    {
        nameLabel.frame = _nameRect;
        addressLabel.frame = _addressRect;
    }*/
    [_starView fitStarsToFrame];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

#pragma mark - Actions
- (IBAction)showAlert:(id)sender
{
   
    NSString *url = [DataManager discountURLForRestaurant: _restaurant.restaurantId];
    [CustomAlert showCustomAlertWithURL: url button: @"סגור"];
}
- (IBAction)showCusieneList:(UIButton*)sender
{
    CGRect buttonRect = sender.frame;
    __weak RestAnnotationCell *weakSelf = self;
//    CGFloat offset = CGRectGetWidth(self.frame) - CGRectGetMaxX(buttonRect);
    buttonRect.origin.x = 14;
    buttonRect.size.width = 143;
    buttonRect.origin.y = 130;
    NSString *title = [sender titleForState: UIControlStateNormal];
    [sender setTitle: @"  " forState: UIControlStateNormal];
    [UIView animateWithDuration: 0.2f animations:^{
        if (sender.selected)
        {
            
            sender.frame = CGRectMake(122, 130, 35, 15);
            [sender setTitle: title forState: UIControlStateNormal];
        }
        else
        {
            sender.frame = buttonRect;
            [sender setTitle: title forState: UIControlStateNormal];
            
        }
        sender.selected = !sender.selected;

    }];
    
    if(sender.selected)
    {
        [Utils handleScreenTouch:^{
            [weakSelf showCusieneList:sender];
        }];
    }
}

- (void)setSelected:(BOOL)selected
{
    
}

- (void)setHighlighted:(BOOL)highlighted
{
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
