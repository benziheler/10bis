//
//  PinAnnotationView.h
//  10bis
//
//  Created by Anton Vilimets on 10/2/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface PinAnnotationView : MKAnnotationView

@end
