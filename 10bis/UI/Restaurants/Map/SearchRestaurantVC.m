//
//  SearchRestaurantVC.m
//  10bis
//
//  Created by Vadim Pavlov on 27.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "SearchRestaurantVC.h"
#import <MapKit/MapKit.h>
#import "DataManager.h"
#import "Restaurant.h"
#import "RestaurantAnnotation.h"
#import "RestaurantMenuVC.h"
#import "RestaurantCell.h"
#import <CoreLocation/CoreLocation.h>
#import "UIView+Extensions.h"
#import "RestaurantVC.h"
#import "Address.h"
#import "StarsView.h"
#import "RestaurantAnnotationView.h"
#import "RestaurantCluster.h"
#import "PinAnnotationView.h"

#import "FilterView.h"

@interface SearchRestaurantVC () <MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (assign, nonatomic) BOOL isCalloutSelected;


@end

@implementation SearchRestaurantVC
{
    __weak IBOutlet MKMapView *_mapView;
    __weak IBOutlet UIView *_listView;
    __weak IBOutlet UITableView *_tableView;
    
    __weak IBOutlet UILabel *_noResultLabel;
    __weak IBOutlet UIButton *_addressButton;
    __weak IBOutlet UIButton *_mapListButton;
    
    __weak IBOutlet UIButton *_filterButton;
    RestaurantAnnotationView *detailView;
    NSDictionary *currFilterDict;
    UITextField *_searchTF;
    UIView *_searchView;
    UISearchBar* _searchBar;
    FilterView *filterView;
    
    CGRect _initialSearchRect;
    
    NSMutableArray *_restaurantsAnnotations;
    
    BOOL _userLocationFound;
    CLLocationCoordinate2D _location;
    CLGeocoder* _geocoder;
    
    __weak Restaurant *_selectedRestaurant;
    
    UIBarButtonItem *_searchItem;
    UIBarButtonItem *_locationItem;
    
    BOOL _restaurantsFound;
    
    NSMutableArray *_filteredRestaurants;
    NSMutableArray *_restaurantsClusters;
    
    NSArray *_requestedBoundary;
    BOOL _isSearchingRestaurants;
    BOOL shouldFollowUser;
    NSMutableArray *sections;
}


- (void)dealloc
{
    _mapView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isCalloutSelected = NO;
    detailView = [RestaurantAnnotationView annotationView];
    detailView.hidden = YES;
    __weak SearchRestaurantVC *weakSelf = self;
    [detailView setDidselectRestaurant:^(Restaurant *restaurant) {
        weakSelf.isCalloutSelected = YES;
        [weakSelf selectRestaurant: restaurant];
        
    }];
    // Do any additional setup after loading the view.
    
    
    _geocoder = [CLGeocoder new];
    [_tableView registerNib: [UINib nibWithNibName: @"RestaurantCell" bundle: nil] forCellReuseIdentifier: @"RestaurantCell"];
    _restaurantsAnnotations = [NSMutableArray arrayWithCapacity: 100];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardShowed:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardHidden) name: UIKeyboardWillHideNotification object: nil];
    
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(tapOnMap:)];
    pan.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(tapOnMap:)];
    tap.delegate = self;
    
    [_mapView addGestureRecognizer: tap];
    [_mapView addGestureRecognizer: pan];
    
    _addressButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _addressButton.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    CLLocationCoordinate2D defaultLocation = [Utils defaultLocation];
    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    
    if (_restaurants)
    {
        [self reloadTable];
        shouldFollowUser = NO;
        _restaurantsFound = YES;
        // already have list of restaurants loaded by address
        NSString *addressString = _addressString;
        if (addressString == nil)
            addressString = _address.addressString;
        
        [_addressButton setTitle: addressString forState: UIControlStateNormal];
        [self addMapAnnotations];
        
        __weak SearchRestaurantVC *weakSelf = self;
        [self geocodeAddress: addressString completion:^(BOOL found) {
            if (found == NO)
            {
                SearchRestaurantVC *strongSelf = weakSelf;
                if (strongSelf && strongSelf->_restaurants.count)
                {
                    Restaurant *firstRestaurant = strongSelf->_restaurants[0];
                    NSString *resAddress = firstRestaurant.street;//[NSString stringWithFormat: @"%@, %@",firstRestaurant.cityName, firstRestaurant.street];
                    [strongSelf geocodeAddress: resAddress completion: nil];
                }
            }
        }];
    }
    else
    {
        // search restaurants by user current location
        shouldFollowUser = YES;
        
        CLLocation *userLocation = [[DataManager sharedManager] currentLocation];
        if (userLocation)
        {
            [self proceedUserLocation: userLocation];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil message: @"אירעה שגיאה בעת קביעת מיקומך.\nאנא וודא/י כי שירותי המיקום מאופשרים." delegate: nil cancelButtonTitle: @"אישור" otherButtonTitles: nil];
            [alert show];
            userLocation = [[CLLocation alloc] initWithLatitude: defaultLocation.latitude longitude: defaultLocation.longitude];
            [self proceedUserLocation: userLocation];
        }
    }
    
    // show list firstly for 'delivery' method
    DeliveryMethod method = [DataManager deliveryMethod];
    NSString *screenTitle = nil;
    if (method == DeliveryMethodDelivery)
    {
        screenTitle = @"הזמנת משלוח";
        _mapListButton.selected = NO;
        [self showMapList: _mapListButton];
    }
    else
        screenTitle = @"מסעדות עם תן ביס";
    self.title = screenTitle;
    
    filterView = [FilterView filterView];
    [self.navigationController.view addSubview:filterView];
    filterView.frame = self.view.bounds;
    filterView.hidden = YES;
    NSDictionary *savedDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
    if(savedDict)
    {
        NSString *cuisineFilter = savedDict[@"cuisine"];
        _filterButton.selected = (cuisineFilter != nil || [savedDict[kKosherKey] boolValue] == YES || [savedDict[kHasDiscountKey] boolValue] == YES);
        
        [filterView setupWithDict:savedDict];
    }
    
    
}

- (void)setRestaurants:(NSArray *)restaurants
{
    _restaurants = restaurants;
    NSDictionary *savedDict = currFilterDict;
    if(!currFilterDict)
    {
        savedDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
    }
    if(savedDict)
    {
        NSString *cuisineFilter = savedDict[@"cuisine"];
        _filterButton.selected = (cuisineFilter != nil || [savedDict[kKosherKey] boolValue] == YES || [savedDict[kHasDiscountKey] boolValue] == YES);
        
        [self filterRestaurantsWithDict:savedDict];
    }
    else if ([DataManager deliveryMethod] == DeliveryMethodPickup || [DataManager deliveryMethod] == DeliveryMethodSitting)
    {
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"distanceFromUser"  ascending:YES];
        NSArray *sorted = [_restaurants sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        _filteredRestaurants = [NSMutableArray arrayWithArray:sorted];
    }
    
    if([DataManager deliveryMethod] == DeliveryMethodDelivery)
    {
        [self buildSections];
    }
}

- (void)buildSections
{
    NSArray *source = _restaurants;
    if(_filteredRestaurants) source = _filteredRestaurants;
    NSArray *company = [source filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(companyFlag == YES)"]];
    NSArray *regular = [source filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(companyFlag == NO)"]];
    sections = [NSMutableArray array];
    if(company.count)
    {
        [sections addObject:company];
    }
    if(regular.count)
    {
        [sections addObject:regular];
    }
}

- (void)reloadTable
{
    if([DataManager deliveryMethod] == DeliveryMethodDelivery)
    {
        [self buildSections];
    }
    else
    {
        sections = nil;
    }
    [_tableView reloadData];
    
}

- (void)geocodeAddress:(NSString*)address completion:(void (^)(BOOL found))handler
{
    __weak SearchRestaurantVC *weakSelf = self;
    [_geocoder geocodeAddressString: address completionHandler:^(NSArray *placemarks, NSError *error) {
        if (placemarks.count)
        {
            CLPlacemark *placemark = [placemarks lastObject];
            [weakSelf zoomMapToCoords: placemark.location.coordinate];
        }
        if (handler)
            handler(placemarks.count > 0);
    }];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    self.isCalloutSelected = NO;
    if(detailView.hidden == NO)
    {
        [detailView.superview.superview bringSubviewToFront:detailView.superview];
    }
    [self configureNavigationBar];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    [_searchView removeFromSuperview];
    _searchView = nil;
    _searchBar = nil;
    _searchTF = nil;
}

- (void)configureNavigationBar
{
    UIButton *searchButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [searchButton setImage: [UIImage imageNamed: @"icon_search"] forState: UIControlStateNormal];
    [searchButton addTarget: self action: @selector(search:) forControlEvents: UIControlEventTouchUpInside];
    [searchButton sizeToFit];
    _searchItem = [[UIBarButtonItem alloc] initWithCustomView: searchButton];
    
    UIButton *locationButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [locationButton setImage: [UIImage imageNamed: @"icn_you_r_here"] forState: UIControlStateNormal];
    [locationButton addTarget: self action: @selector(showMe) forControlEvents: UIControlEventTouchUpInside];
    [locationButton sizeToFit];
    _locationItem = [[UIBarButtonItem alloc] initWithCustomView: locationButton];
    
    [self.navigationItem setRightBarButtonItem: _mapListButton.selected ? _searchItem : _locationItem animated: YES];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextAlignment:NSTextAlignmentRight];
    _initialSearchRect = CGRectMake(0, -64, DEVICE_WIDTH, 44);
    _searchView = [[UIView alloc] initWithFrame: _initialSearchRect];
    _searchView.backgroundColor = [UIColor clearColor];
    _searchBar = [[UISearchBar alloc] initWithFrame: _searchView.bounds];
    
    _searchBar.showsCancelButton = YES;
    _searchBar.backgroundImage = [UIImage alloc];
    _searchBar.backgroundColor = [Utils appRedColor];
    _searchBar.tintColor = [Utils appRedColor];
    _searchBar.delegate = self;
    [_searchView addSubview: _searchBar];
    
    if ([Utils isIOS7])
    {
        [searchButton sizeToFit];
    }
    else
    {
        searchButton.frame = CGRectMake(0, 0, 30, 25);
    }
    
    
    UIButton *cancelButton = [UIButton buttonWithType: UIButtonTypeCustom];
    cancelButton.backgroundColor = [Utils appRedColor];
    [cancelButton setTitle: @"ביטול" forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
    [cancelButton setTitleColor: [UIColor lightGrayColor] forState: UIControlStateHighlighted];
    
    [cancelButton addTarget: self action: @selector(cancelButtonClicked) forControlEvents: UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(DEVICE_WIDTH - 55, 5, 50, 34);
    cancelButton.titleLabel.font = [UIFont fontWithName: @"Arial-BoldMT" size: 15];
    [_searchBar addSubview: cancelButton];
    _searchBar.tintColor = [Utils appRedColor];
    [self.navigationController.navigationBar addSubview: _searchView];
    
    for (UIView *subView in _searchBar.subviews){
        if ([subView isKindOfClass: [UITextField class]])
            _searchTF =  (UITextField *)subView;
        else
            for (UIView *secondLevelSubView in subView.subviews)
            {
                if ([secondLevelSubView isKindOfClass:[UITextField class]])
                {
                    _searchTF = (UITextField *)secondLevelSubView;
                    break;
                }
            }
        if (_searchTF)
        {
            _searchTF.enablesReturnKeyAutomatically = NO;
            _searchTF.textColor = [Utils appRedColor];
            _searchTF.background = [UIImage alloc];
            _searchTF.borderStyle = UITextBorderStyleNone;
            _searchTF.backgroundColor = RGB(228, 227, 223);
            _searchTF.layer.cornerRadius = 2.0f;
            
            if ([Utils isIOS7])
                _searchTF.tintColor = [Utils appRedColor];
            else
                [[_searchTF valueForKey:@"textInputTraits"] setValue: [Utils appRedColor] forKey:@"insertionPointColor"];
            break;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString: @"restaurantMenuSegue"])
    {
        RestaurantMenuVC *menuVC = segue.destinationViewController;
        menuVC.restaurant = _selectedRestaurant;
        
        DeliveryMethod method = [DataManager deliveryMethod];
        if ((method == DeliveryMethodPickup || method == DeliveryMethodSitting)
            && _userLocationFound)
        {
            menuVC.curLocation = _location;
        }
    }
    else if ([segue.identifier isEqualToString: @"restaurantSegue"])
    {
        RestaurantVC *restaurantVC = segue.destinationViewController;
        restaurantVC.restaurant = _selectedRestaurant;
    }
}

- (void)proceedUserLocation:(CLLocation*)userLocation
{
    
    _location = userLocation.coordinate;
    [self reloadTable]; // will update distance calculation
    
    if (_userLocationFound == NO)
    {
        _userLocationFound = YES;
        if (_restaurantsFound == NO) // restaurants was not found by city/street
        {
            [self zoomMapToCoords: userLocation.coordinate];
            _mapView.userTrackingMode = MKUserTrackingModeFollow;
            [self searchRestaurantsByLocation: _location];
        }
    }
    // update user address
    if ([_geocoder isGeocoding])
        return;
    
    __weak SearchRestaurantVC *weakSelf = self;
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    
    [_geocoder reverseGeocodeLocation: userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        SearchRestaurantVC *strongSelf = weakSelf;
        if (strongSelf && placemarks.count)
        {
            CLPlacemark *placemark = [placemarks lastObject];
            NSString * addressString = [NSString stringWithFormat: @"%@, %@", placemark.locality,placemark.thoroughfare];
            if(!placemark.thoroughfare)
            {
                addressString = [NSString stringWithFormat: @"%@", placemark.locality];
                
            }
            [strongSelf->_addressButton setTitle: addressString forState: UIControlStateNormal];
            
        }
        else
        {
            [strongSelf->_addressButton setTitle:@"כתובת לא זמינה" forState: UIControlStateNormal];
        }
    }];
}
#pragma mark - Restaurants Clusters
- (void)updateRestaurantsClustersFromRestaurants:(NSArray*)restaurants
{
    CLLocationDistance distance = 50.0f;
    _restaurantsClusters = [NSMutableArray arrayWithCapacity: restaurants.count];
    
    for (Restaurant *restaurant in restaurants)
    {
        CLLocation *restaurantLoc = [[CLLocation alloc] initWithLatitude: restaurant.latitude longitude: restaurant.longitude];
        BOOL joinedCluster = NO;
        
        for (RestaurantCluster *cluster in _restaurantsClusters)
        {
            CLLocationDistance clusterDistance = [cluster.location distanceFromLocation: restaurantLoc];
            if (clusterDistance < distance)
            {
                joinedCluster =  YES;
                [cluster.restaurants addObject: restaurant];
                break;
            }
        }
        
        if (joinedCluster == NO)
        {
            RestaurantCluster *cluster = [RestaurantCluster new];
            cluster.location = restaurantLoc;
            cluster.restaurants = [NSMutableArray arrayWithCapacity: 10];
            [cluster.restaurants addObject: restaurant];
            [_restaurantsClusters addObject: cluster];
        }
        
    }
}

#pragma mark - Actions
- (void)keyboardShowed:(NSNotification*)notification
{
    NSValue* aValue     = [[notification userInfo] objectForKey: UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
    _tableView.scrollIndicatorInsets = _tableView.contentInset;
}
- (void)keyboardHidden
{
    _tableView.contentInset = UIEdgeInsetsZero;
    _tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}

- (IBAction)changeAddress:(id)sender
{
    UINavigationController *naviVC = self.navigationController;
    UIViewController *addressesVC = [self.storyboard instantiateViewControllerWithIdentifier: @"AddressesVC"];
    [naviVC setViewControllers:@[naviVC.viewControllers.firstObject, addressesVC] animated:YES];
}
- (IBAction)showMapList:(UIButton*)sender
{
    if (sender.selected)
    {
        _mapView.hidden = NO;
        _listView.hidden = YES;
        [self.navigationItem setRightBarButtonItem: _locationItem animated: YES];
        [_searchBar resignFirstResponder];
        [UIView animateWithDuration: 0.3f animations:^{
            _searchView.frame = _initialSearchRect;
        }];
        
    }
    else
    {
        _mapView.hidden = YES;
        _listView.hidden = NO;
        [self.navigationItem setRightBarButtonItem: _searchItem animated: YES];
    }
    sender.selected = !sender.selected;
}

- (void)search:(id)sender
{
    [UIView animateWithDuration: 0.3f animations:^{
        CGRect searchRect = _searchView.frame;
        searchRect.origin.y = 0;
        _searchView.frame = searchRect;
    } completion:^(BOOL finished) {
        [_searchTF becomeFirstResponder];
    }];
}

- (void)selectRestaurant:(Restaurant*)restaurant
{
    _selectedRestaurant = restaurant;
    
    NSString *segue = @"restaurantMenuSegue"; // make currently all methods open screen with menu
    /*
     DeliveryMethod method = [DataManager deliveryMethod];
     switch (method)
     {
     case DeliveryMethodPickup:
     case DeliveryMethodDelivery:
     segue = @"restaurantMenuSegue";
     break;
     case DeliveryMethodSitting:
     segue = @"restaurantSegue";
     break;
     default:
     NSLog(@"Select reustarant for undefined delivery method");
     break;
     }
     */
    if (segue)
        [self performSegueWithIdentifier: segue sender: self];
    
}
#pragma mark - Search Delegate
- (void)cancelButtonClicked
{
    [self resetSearch];
    [_searchBar resignFirstResponder];
    [UIView animateWithDuration: 0.3f animations:^{
        _searchView.frame = _initialSearchRect;
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if (searchBar.text.length == 0)
    {
        [self resetSearch];
    }
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        [self resetSearch];
    }
    else
    {
        NSMutableArray * resultArr = [NSMutableArray arrayWithCapacity: _restaurants.count];
        NSArray *allRestaurants = _restaurants;
        if(currFilterDict)
        {
            allRestaurants = [self filteredRestaurantsArrayWithDict:currFilterDict];
        }
        for (Restaurant *restaurant in allRestaurants)
        {
            if ([self restaurant: restaurant containsSearchString: searchText])
            {
                [resultArr addObject: restaurant];
            }
        }
        _filteredRestaurants = resultArr;
        [self reloadTable];
        [_tableView setContentOffset:CGPointZero];
        [self addMapAnnotations];
    }
}
- (BOOL)restaurant:(Restaurant*)restaurant containsSearchString:(NSString*)searchText
{
    NSMutableArray *searchInStrings = [NSMutableArray arrayWithCapacity: 4];
    if (restaurant.name)
        [searchInStrings addObject: restaurant.name];
    if (restaurant.cityName)
        [searchInStrings addObject: restaurant.cityName];
    if (restaurant.street)
        [searchInStrings addObject: restaurant.street];
    if (restaurant.cuisineList)
        [searchInStrings addObject: restaurant.cuisineList];
    
    for (NSString *candidateString in searchInStrings)
    {
        NSRange range = [candidateString rangeOfString: searchText options: (NSCaseInsensitiveSearch)];
        if (range.location != NSNotFound)
        {
            return YES;
        }
    }
    return NO;
}

- (void)resetSearch
{
    _searchTF.text = nil;
    _filteredRestaurants = nil;
    if(currFilterDict)
    {
        _filteredRestaurants = [self filteredRestaurantsArrayWithDict:currFilterDict];
    }
    [self reloadTable];
    [_tableView setContentOffset:CGPointZero animated:1];
    [self addMapAnnotations];
}

- (void)searchRestaurantsByLocation:(CLLocationCoordinate2D)location
{
    __weak SearchRestaurantVC *weakSelf = self;
    [self showLoadingOverlay];
    _isSearchingRestaurants = YES;
    [DataManager searchRestaurantsByMap: location completion:^(id result, NSString *error) {
        [weakSelf removeLoadingOverlay];
        SearchRestaurantVC *strongSelf = weakSelf;
        if (strongSelf)
        {
            strongSelf->_isSearchingRestaurants = NO;
            strongSelf->_requestedBoundary = [Utils requestBoundariesForLocation: location];
            strongSelf.restaurants = result;
            [strongSelf addMapAnnotations];
            [strongSelf reloadTable];
        }
    }];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    if (restaurantsArray && restaurantsArray.count == 0)
        _noResultLabel.hidden = NO;
    else
        _noResultLabel.hidden = YES;
    if(sections)
    {
        return sections.count;
    }
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    
    if(sections)
    {
        restaurantsArray = sections[section];
    }
    
    return restaurantsArray.count;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RestaurantCell"];
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    if(sections)
    {
        restaurantsArray = sections[indexPath.section];
    }
    cell.restaurant = restaurantsArray[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    NSArray *restaurantsArray = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    if(sections)
    {
        restaurantsArray = sections[indexPath.section];
    }
    [self selectRestaurant: restaurantsArray[indexPath.row]];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10.0f;
}
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame: CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return sections ?  35.0f : 0;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(sections)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH + 4, 35)];
        view.backgroundColor = [UIColor clearColor];
        CGRect frame = view.bounds;
        frame.size.height = 30;
        UILabel *lbl = [[UILabel alloc] initWithFrame:frame];
        [view addSubview:lbl];
        lbl.backgroundColor = RGB(199, 36, 34);
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.textColor = [UIColor whiteColor];
        NSArray *arr = sections[section];
        Restaurant *rest = arr.firstObject;
        if(rest.companyFlag)
        {
            lbl.text = @"הזמנה מרוכזת";
        }
        else
        {
            lbl.text = @"הזמנה אישית";
        }
        // NSDictionary *obj = [self.currentDatasource[section] firstObject];
//[self headerTitleForObject:obj];
        return view;
    }
    return nil;
}

#pragma mark - Map View
- (void)showMe
{
    //_mapView.showsUserLocation = NO;
    if(detailView.hidden == NO)
    {
        detailView.hidden = YES;
    }
    CLLocation *userLocation = [[DataManager sharedManager] currentLocation];
    if (CLLocationCoordinate2DIsValid(_location))
        [self zoomMapToCoords: _location];
    else if (userLocation)
        [self zoomMapToCoords: userLocation.coordinate];
    
}
- (void)tapOnMap:(id)sender
{
    if(detailView.hidden == NO)
    {
        detailView.hidden = YES;
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView: _mapView];
    UIView *view = [_mapView hitTest: point withEvent: nil];
    if ([view isKindOfClass: [MKAnnotationView class]])
        return NO;
    UIView *superView = view.superview;
    while (superView)
    {
        if ([superView isKindOfClass: [MKAnnotationView class]] || [superView isKindOfClass:[UICollectionViewCell class]])
            return NO;
        superView = superView.superview;
        if (superView == detailView || [view isKindOfClass:[MKAnnotationView class]])
        {
            return NO;
        }
    }
    return YES;
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)zoomMapToCoords:(CLLocationCoordinate2D)coords
{
    double miles = 0.43f;
    double scalingFactor = ABS(cos (2 * M_PI * coords.latitude /360.0));
    MKCoordinateSpan span;
    span.latitudeDelta = miles/69.0;
    span.longitudeDelta = miles/(scalingFactor*69.0);
    MKCoordinateRegion region;
    region.span = span;
    region.center = coords;
    [_mapView setRegion: region animated: YES];
}
- (void)addMapAnnotations
{
    
    [_mapView removeAnnotations: _mapView.annotations];
    NSArray *restaurants = _filteredRestaurants ? _filteredRestaurants : _restaurants;
    // group restaurants in clusters
    [self updateRestaurantsClustersFromRestaurants: restaurants];
    
    NSMutableArray* annotaions = [NSMutableArray array];
    for (RestaurantCluster *cluster in _restaurantsClusters)
    {
        RestaurantAnnotation *annotation = nil;
        if (cluster.restaurants.count == 1)
        {
            Restaurant *restaurant = [cluster.restaurants lastObject];
            annotation = [[RestaurantAnnotation alloc] initWithRestaurant: restaurant];
        }
        else
        {
            annotation = [[RestaurantAnnotation alloc] initWithCluster: cluster];
        }
        if (CLLocationCoordinate2DIsValid(annotation.coordinate))
        {
            [_restaurantsAnnotations addObject: annotation];
            [annotaions addObject: annotation];
        }
    }
    
    [_mapView addAnnotations: annotaions];
}


- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(RestaurantAnnotation *)annotation
{
    
    
    if([annotation isKindOfClass:[MKUserLocation class]])
    {
        MKAnnotationView *userLocView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"userLoc"];
        
        userLocView.canShowCallout = NO;
        
        UILabel *hereLabel = [[UILabel alloc] initWithFrame: CGRectMake(-3, -45, 30, 50)];
        hereLabel.numberOfLines = 3;
        hereLabel.text = @"You are here";
        hereLabel.backgroundColor = [UIColor clearColor];
        hereLabel.textColor = [Utils appRedColor];
        hereLabel.font = [UIFont fontWithName: @"Arial" size: 12];
        hereLabel.textAlignment = NSTextAlignmentCenter;
        [userLocView addSubview: hereLabel];
        
        userLocView.image = [UIImage imageNamed: @"icon_you_are_here"];
        
        return userLocView;
    }
    MKAnnotationView *annotationView = nil;
    NSInteger numberOfRestaurans = annotation.cluster.restaurants.count;
    
    // Pin Annotation
    annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier: @"RestaurantPinAnnotation"];
    if (annotationView == nil)
    {
        annotationView = [[PinAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"RestaurantPinAnnotation"];
        annotationView.exclusiveTouch = YES;
        NSArray *xib = [[NSBundle mainBundle] loadNibNamed: @"RestaurantPinAnnotation" owner: self options: nil];
        UIView *view = xib[0];
        [annotationView addSubview: view];
        annotationView.frame = view.bounds;
    }
    annotationView.annotation = annotation;
    
    UIView *logoView = [annotationView viewWithTag: 1];
    UILabel *pinLabel = (UILabel*)[annotationView viewWithTag: 2];
    logoView.hidden = numberOfRestaurans != 0;
    pinLabel.hidden = numberOfRestaurans == 0;
    pinLabel.text = [NSString stringWithFormat: @"%ld", (long)numberOfRestaurans];
    
    return annotationView;
}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if(self.isCalloutSelected == NO)
    {
        [mapView deselectAnnotation: view.annotation animated: NO];
        
        
        if ([view.reuseIdentifier isEqualToString: @"RestaurantPinAnnotation"])
        {
            RestaurantAnnotation *pinAnnotation = (RestaurantAnnotation*)view.annotation;
            
            
            RestaurantAnnotation *annotation = nil;
            if(pinAnnotation.cluster)
            {
                annotation = [[RestaurantAnnotation alloc] initWithCluster:pinAnnotation.cluster];
            }
            else
            {
                annotation = [[RestaurantAnnotation alloc ]initWithRestaurant:pinAnnotation.restaurant];
            }
            detailView.annotation = annotation;
            detailView.hidden = NO;
            [view addSubview:detailView];
            CGRect frame = detailView.frame;
            frame.origin.x = - frame.size.width + 30;
            frame.origin.y = - frame.size.height;
            detailView.frame = frame;
            annotation.isShowed = YES;
            MKMapPoint point = MKMapPointForCoordinate(annotation.coordinate);
            if([UIScreen mainScreen].bounds.size.height == 480)
            {
                point.y -= 250;
            }
            CGFloat offset = [Utils isIOS7] ? 350.0f : 200.0f;
            MKMapRect rect = MKMapRectMake(point.x - offset, point.y, 0, 0);
            [mapView setVisibleMapRect: rect animated: YES];
        }
    }
    else
    {
        [detailView.superview.superview bringSubviewToFront:detailView.superview];
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if(shouldFollowUser)
    {
        DeliveryMethod method = [DataManager deliveryMethod];
        if (method == DeliveryMethodPickup || method == DeliveryMethodSitting)
        {
            [self proceedUserLocation: userLocation.location];
        }
    }
    _userLocationFound = YES;
    _location = userLocation.coordinate;
    
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (_requestedBoundary && _isSearchingRestaurants == NO)
    {
        MKCoordinateRegion region =  mapView.region;
        CLLocationDegrees visibleNorth = region.center.latitude + region.span.latitudeDelta/2.0f;
        CLLocationDegrees visibleSouth = region.center.latitude - region.span.latitudeDelta/2.0f;
        CLLocationDegrees visibleEast = region.center.longitude + region.span.longitudeDelta/2.0f;
        CLLocationDegrees visibleWest = region.center.longitude - region.span.longitudeDelta/2.0f;
        
        CLLocationDegrees requestedNorth = [_requestedBoundary[0] doubleValue];
        CLLocationDegrees requestedSouth = [_requestedBoundary[1] doubleValue];
        CLLocationDegrees requestedEast = [_requestedBoundary[2] doubleValue];
        CLLocationDegrees requestedWest = [_requestedBoundary[3] doubleValue];
        
        if ((visibleNorth - visibleSouth) > (requestedNorth - requestedSouth) ||
            (visibleEast - visibleWest) > (requestedEast - requestedWest))
        {
            NSLog(@"return");
            return;
        }
        
        if (visibleNorth > requestedNorth ||
            visibleSouth < requestedSouth ||
            visibleEast > requestedEast ||
            visibleWest < requestedWest)
        {
            NSLog(@"search");
            [self searchRestaurantsByLocation: region.center];
        }
    }
}
- (IBAction)showFilterScreen:(id)sender {
    [_searchBar resignFirstResponder];
    __weak SearchRestaurantVC *weakSelf = self;
    filterView.isSortingEnabled = _mapView.hidden;
    if(currFilterDict)
    {
        [filterView setupWithDict:currFilterDict];
    }
    [filterView showWithCompletion:^(NSDictionary *result) {
        if(result || currFilterDict)
        {
            NSDictionary *dict = result ? result : currFilterDict;
            NSString *cuisineFilter = dict[@"cuisine"];
            
            _filterButton.selected = (cuisineFilter != nil || [dict[kKosherKey] boolValue] == YES || [dict[kHasDiscountKey] boolValue]);
        }
        else
        {
            _filterButton.selected = NO;
        }
        [weakSelf filterRestaurantsWithDict:result];
    }];
}

- (void)filterRestaurantsWithDict:(NSDictionary *)result
{
    if(result)
    {
        currFilterDict = result;
    }
    NSMutableArray *filtered = [self filteredRestaurantsArrayWithDict:currFilterDict];
    _filteredRestaurants = filtered;
    [self reloadTable];
    [_tableView setContentOffset:CGPointZero];
    if(detailView.hidden == NO)
    {
        detailView.hidden = YES;
    }
    [self addMapAnnotations];
}

- (NSMutableArray *)filteredRestaurantsArrayWithDict:(NSDictionary *)result
{
    NSMutableArray *resultArr = nil;
    if(result == nil)
    {
        NSDictionary *savedDict = nil;//[[NSUserDefaults standardUserDefaults] dictionaryForKey:[DataManager filterSettingKey]];
        if(savedDict)
        {
            NSString *cuisineFilter = savedDict[@"cuisine"];
            _filterButton.selected = (cuisineFilter != nil || [savedDict[kKosherKey] boolValue] == YES || [savedDict[kHasDiscountKey] boolValue] == YES);
            
            [filterView setupWithDict:savedDict];
            resultArr = [self filteredRestaurantsArrayWithDict:savedDict];
        }
        else
        {
            _filteredRestaurants = nil;
            [self reloadTable];
            [_tableView setContentOffset:CGPointZero];
            if(detailView.hidden == NO)
            {
                detailView.hidden = YES;
            }
            [self addMapAnnotations];
            
        }
        resultArr = [_restaurants mutableCopy];
    }
    resultArr = [NSMutableArray arrayWithCapacity: _restaurants.count];
    if (result[@"cuisine"])
    {
        NSString *cuisineName = [result[@"cuisine"] objectForKey:@"CuisineTypeName"];
        NSArray *filtered = [_restaurants filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(cuisineList contains[cd] %@)", cuisineName]];
        resultArr = [NSMutableArray arrayWithArray:filtered];
    }
    else
    {
        [resultArr addObjectsFromArray:_restaurants];
    }
    NSString *key = @"sortByPickup";
    if([DataManager deliveryMethod] == DeliveryMethodDelivery)
    {
        key = @"sortByDelivery";
    }
    if(result[key])
    {
        BOOL ascending = NO;
        NSString *sortValue = [result[key] objectForKey:@"field"];
        if([sortValue isEqualToString:@"minimumPriceForOrder"] || [sortValue isEqualToString:@"deliveryPriceForOrder"] || [sortValue isEqualToString:@"name"] || [sortValue isEqualToString:@"distanceFromUser"])
        {
            ascending = YES;
        }
        if([sortValue isEqualToString:@"discountCouponPercent"] && [DataManager deliveryMethod] == DeliveryMethodSitting)
        {
            sortValue = @"happyHourDiscountPercent";
        }
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:sortValue  ascending:ascending];
        NSArray *sorted = [resultArr sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        resultArr = [NSMutableArray arrayWithArray:sorted];
    }
    if([result[kKosherKey] boolValue])
    {
        NSArray *filtered = [resultArr filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(isKosher == %@)", @"Kosher"]];
        resultArr = [NSMutableArray arrayWithArray:filtered];
    }
    
    if([result[kHasDiscountKey] boolValue])
    {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(discountCouponPercent>0)"];
        if([DataManager deliveryMethod] == DeliveryMethodSitting)
        {
            pred = [NSPredicate predicateWithFormat:@"(isHappyHourActive == YES)"];
        }
        NSArray *filtered = [resultArr filteredArrayUsingPredicate:pred];
        resultArr = [NSMutableArray arrayWithArray:filtered];
    }
    return resultArr;
}

@end
