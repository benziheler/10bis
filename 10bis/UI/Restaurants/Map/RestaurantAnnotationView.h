//
//  RestaurantAnnotationView.h
//  10bis
//
//  Created by Vadim Pavlov on 29.11.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RestaurantAnnotation;
@class Restaurant;
@interface RestaurantAnnotationView : UIView <UICollectionViewDataSource, UICollectionViewDelegate>
+ (id)annotationView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) RestaurantAnnotation *annotation;
@property (nonatomic, copy) void (^didselectRestaurant)(Restaurant *);

@end
