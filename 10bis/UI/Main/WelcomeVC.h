//
//  WelcomeVC.h
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "BaseVC.h"
@interface WelcomeVC : BaseVC

@property (assign, nonatomic) BOOL needToResetDeliveryMethod;
@property (weak, nonatomic) IBOutlet UIButton *pickupButton;
@property (weak, nonatomic) IBOutlet UIButton *eatoutButton;

@end
