//
//  WelcomeVC.m
//  10bis
//
//  Created by Vadim Pavlov on 22.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "WelcomeVC.h"
#import "DataManager.h"
#import "ECSlidingViewController.h"
#import "GigyaVC.h"
#import "CreateAccountVC.h"
@interface WelcomeVC ()

@end

@implementation WelcomeVC
{
    __weak IBOutlet UIButton *_deliveryButton;
    __weak IBOutlet UIImageView *_bgImageView;
    BOOL _isTransiting;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _needToResetDeliveryMethod = YES;
	// Do any additional setup after loading the view.
    _bgImageView.image = [UIImage imageNamed: @"bg"]; // will set 4inch image if needed
    
    UIButton *menuButton = [Utils menuButton];
    [menuButton addTarget: self action: @selector(menuButtonTapped:) forControlEvents: UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithCustomView: menuButton];
    self.navigationItem.leftBarButtonItem = menuItem;
//    _deliveryButton.imageView.layer.cornerRadius = 4.0f;
    NSString *imgName = @"img_delivey";
    if([UIScreen mainScreen].bounds.size.height == 568) imgName = @"img_delivey-568h@2x.png";
    else if([UIScreen mainScreen].bounds.size.height > 568) imgName = @"img_delivey@3x";
    UIImage *img =  [UIImage imageNamed: imgName];
    [_deliveryButton setImage: img forState: UIControlStateNormal]; // will update image for 4inch if needed
    
    _deliveryButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _pickupButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _eatoutButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _pickupButton.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    _pickupButton.imageView.frame = _pickupButton.bounds;

    NSString *pickupImgName = @"img_pickup", *eatoutImgName = @"img_eatout";
    
    if([UIScreen mainScreen].bounds.size.height > 568)
    {
        pickupImgName = [pickupImgName stringByAppendingString:@"@3x"];
                eatoutImgName = [eatoutImgName stringByAppendingString:@"@3x"];
    }
     [_pickupButton setImage: [UIImage imageNamed:pickupImgName] forState: UIControlStateNormal];
         [_eatoutButton setImage: [UIImage imageNamed:eatoutImgName] forState: UIControlStateNormal];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"logo_main"]];
    self.navigationItem.titleView = logo;
    if ([DataManager isAuthorizationComplete] == NO)
    {
        UIImageView *splash = [[UIImageView alloc] initWithFrame: self.view.bounds];
        NSString *imgName = @"LaunchImage-700-568h@2x.png";
        if([UIScreen mainScreen].bounds.size.height == 480)
        {
            imgName = @"splash";
        }
        else if(IS_IPHONE_6PLUS)
        {
            imgName = @"LaunchImage-800-Portrait-736h@3x.png";
        }
        else if([UIScreen mainScreen].bounds.size.height == 568)
        {
            imgName = @"LaunchImage-700-568h@2x.png";
        }
        else
        {
            imgName = @"LaunchImage-800-667h@2x.png";
        }

        splash.image = [UIImage imageNamed: imgName];
        splash.backgroundColor = [UIColor cyanColor];
        [self.navigationController.navigationBar addSubview: splash];
        
        [[NSNotificationCenter defaultCenter] addObserverForName: kUserAuthorizationCompleteNotificationKey object: nil queue: nil usingBlock:^(NSNotification *note) {
            [self showStatusBar];
            [UIView animateWithDuration: 0.2f animations:^{
                splash.alpha = 0.0f;
            } completion:^(BOOL finished) {
                [splash removeFromSuperview];
            }];
        }];
    }
    else
        [self showStatusBar];
}
- (void)showStatusBar
{
    UIApplication *application = [UIApplication sharedApplication];
    [application setStatusBarHidden: NO withAnimation: UIStatusBarAnimationSlide];

}
- (void)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    if(_needToResetDeliveryMethod)
    {
        [DataManager setDeliveryMethod: DeliveryMethodNone];
    }
    _needToResetDeliveryMethod= YES;
    [DataManager setQuickOrder: NO];
    [DataManager setLoginForCheckout: NO];
    [DataManager setSearchCityID: nil];
    [DataManager setSearchStreetID: nil];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear: animated];
    _isTransiting = NO;
}

#pragma mark - Actions
- (id <GAITracker>)tracker
{
    return [[GAI sharedInstance] defaultTracker];
}
- (IBAction)pickup:(id)sender
{
    if (_isTransiting)
        return;
    _isTransiting = YES;
    [DataManager setDeliveryMethod: DeliveryMethodPickup];
    
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                          action:@"button_press"
                                                           label:@"Pickup"
                                                           value:nil] build]];
    
    [self performSegueWithIdentifier: @"searchRestaurantsSegue" sender: self];

}
- (IBAction)visit:(id)sender
{
    if (_isTransiting)
        return;
    _isTransiting = YES;
    [DataManager setDeliveryMethod: DeliveryMethodSitting];
    
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                               action:@"button_press"
                                                                label:@"Eat Out"
                                                                value:nil] build]];
    [self performSegueWithIdentifier: @"searchRestaurantsSegue" sender: self];
}
- (IBAction)delivery:(id)sender
{
    if (_isTransiting)
        return;
    _isTransiting = YES;

    [DataManager setDeliveryMethod: DeliveryMethodDelivery];
    
    [self.tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
                                                               action:@"button_press"
                                                                label:@"Delivery"
                                                                value:nil] build]];

    if ([DataManager currentUser] == nil)
    {
        [self performSegueWithIdentifier: @"loginSegue" sender: self];
        return;
    }
    [self performSegueWithIdentifier: @"addressSegue" sender: self];
}
@end
