//
//  TransactionReportVC.h
//  10bis
//
//  Created by Anton Vilimets on 9/26/14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "SKSTableView.h"

@interface TransactionReportVC : BaseVC <SKSTableViewDelegate>
@property (weak, nonatomic) IBOutlet SKSTableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *prevButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIButton *prevLarge;
@property (strong, nonatomic) IBOutlet UIButton *nextLarge;
@property (strong, nonatomic) IBOutlet UILabel *noResLabel;
@property (strong, nonatomic) NSMutableArray *currentDatasource;
- (IBAction)prevPressed:(id)sender;
- (IBAction)nextPressed:(id)sender;

@end
