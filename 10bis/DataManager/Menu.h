//
//  Menu.h
//  10bis
//
//  Created by Vadim Pavlov on 28.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Menu : NSObject
@property (nonatomic, strong) NSString *categoryDesc;
@property (nonatomic, strong) NSNumber *categoryId;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSArray *dishList;
@property (nonatomic, assign) BOOL isCategorySelected;

+ (id)menuWithData:(NSDictionary*)menuData;

@end
