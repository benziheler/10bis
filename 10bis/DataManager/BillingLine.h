//
//  BillingLine.h
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, BillingLineType)
{
    BillingLineTip,
    BillingLineSubTotal,
    BillingLineTax,
    BillingLineDeliveryCharge,
    BillingLineDiscountCoupon,
    BillingLineTotalToCharge,
    BillingLineDeliveryDiscount,
};
@interface BillingLine : NSObject
@property (nonatomic, assign) BillingLineType type;
@property (nonatomic, strong) NSNumber *amount;
@property (nonatomic, strong) NSString *amountString;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) BOOL isImportantPaymentType;

+ (instancetype)billingTypeFromData:(NSDictionary*)data;
@end
