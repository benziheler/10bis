//
//  Street.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Street : NSObject
@property (nonatomic, strong) NSNumber *streetId;
@property (nonatomic, strong) NSString *streetName;

+ (id)streetWithData:(NSDictionary*)streetData;
@end
