//
//  RestaurantCluster.h
//  10bis
//
//  Created by Vadim Pavlov on 04.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantCluster : NSObject
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) NSMutableArray *restaurants;
@end
