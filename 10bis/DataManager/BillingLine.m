//
//  BillingLine.m
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "BillingLine.h"

@implementation BillingLine
+ (instancetype)billingTypeFromData:(NSDictionary*)data
{
    BillingLine *line = [BillingLine new];
    line.type = [ValueOrNil(data, @"Type") integerValue];
    line.amount = ValueOrNil(data, @"Amount");
    line.amountString = ValueOrNil(data, @"AmountStr");
    line.caption = ValueOrNil(data, @"Caption");
    line.index = [ValueOrNil(data, @"Index") integerValue];
    line.isImportantPaymentType = [ValueOrNil(data, @"IsImportantPaymentType") boolValue];
    
    return line;
}
@end
