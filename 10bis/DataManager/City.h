//
//  City.h
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject
@property (nonatomic, strong) NSNumber *cityId;
@property (nonatomic, assign) BOOL isBigCity;
@property (nonatomic, strong) NSString *cityName;

+ (id)cityWithData:(NSDictionary*)cityData;
@end
