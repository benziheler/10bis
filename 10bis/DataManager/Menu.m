//
//  Menu.m
//  10bis
//
//  Created by Vadim Pavlov on 28.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "Menu.h"
#import "Dish.h"
@implementation Menu
+ (id)menuWithData:(NSDictionary*)menuData
{
    Menu *menu = [Menu new];
    menu.categoryDesc = ValueOrNil(menuData, @"CategoryDesc");
    menu.categoryId = ValueOrNil(menuData, @"CategoryID");
    menu.categoryName = ValueOrNil(menuData, @"CategoryName");
    menu.isCategorySelected = [ValueOrNil(menuData, @"IsCategorySelected") boolValue];
    
    NSArray *dishListData = ValueOrNil(menuData, @"DishList");
    if ([dishListData isKindOfClass: [NSArray class]])
    {
        NSMutableArray *dishList = [NSMutableArray arrayWithCapacity: dishListData.count];
        for (NSDictionary *dishData in dishListData)
        {
            Dish *dish = [Dish dishWithData: dishData];
            [dishList addObject: dish];
        }
        menu.dishList = dishList;
    }
    return menu;
}
@end
