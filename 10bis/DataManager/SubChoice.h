//
//  SubChoice.h
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubChoice : NSObject <NSCopying>
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *price;
+ (instancetype)subChoiceWithData:(NSDictionary*)data;
@end
