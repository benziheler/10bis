//
//  OrderConfirmation.h
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderConfirmation : NSObject
@property (nonatomic, strong) NSArray *dishes;
@property (nonatomic, strong) NSArray *billingLines;
@property (nonatomic, assign) BOOL showTipSelection;
@property (nonatomic, strong) NSNumber *restaurantId;
@property (nonatomic, strong) NSArray *tipValues;
+ (instancetype)orderConfirmationWithData:(NSDictionary*)data;
@end
