//
//  Order.h
//  10bis
//
//  Created by Vadim Pavlov on 27.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject
@property (nonatomic, strong) NSNumber* ID;
@property (nonatomic, strong) NSNumber* dishOwnerId;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, strong) NSNumber* assignedUserId;
@property (nonatomic, strong) NSString *dishNotes;
@property (nonatomic, strong) NSArray *choices;
@property (nonatomic, strong) NSNumber *orderIndex;
@end
