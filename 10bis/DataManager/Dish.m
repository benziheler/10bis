//
//  Dish.m
//  10bis
//
//  Created by Vadim Pavlov on 28.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "Dish.h"
#import "Choice.h"
@implementation Dish
+ (id)dishWithData:(NSDictionary*)dishData
{
    Dish *dish = [Dish new];
    dish.categoryId = ValueOrNil(dishData, @"CategoryID");
    dish.categoryName = ValueOrNil(dishData, @"CategoryName");
    
    dish.dishDesc = ValueOrNil(dishData, @"DishDescription");
    dish.dishHasImage = [ValueOrNil(dishData, @"DishHasImage") boolValue];
    dish.dishId = ValueOrNil(dishData, @"DishId");
    
    dish.dishImageURL = ValueOrNil(dishData, @"DishImageUrl");
    dish.dishName = ValueOrNil(dishData, @"DishName");
    dish.dishPrice = ValueOrNil(dishData, @"DishPrice");
    dish.minimalQuantity = [ValueOrNil(dishData, @"MinimalQuantity") integerValue];
    dish.thereAreNoChoices = [ValueOrNil(dishData, @"ThereAreNoChoices") boolValue];
    NSArray *choicesData = ValueOrNil(dishData, @"Choices");
    if ([choicesData isKindOfClass: [NSArray class]])
    {
        NSMutableArray *choices = [NSMutableArray arrayWithCapacity: choicesData.count];
        for (NSDictionary *choiceData in choicesData)
        {
            Choice *choice = [Choice choiceWithData: choiceData];
            [choices addObject: choice];
        }
        dish.choices = choices;
    }
    
    NSDictionary *bookmarkDict = ValueOrNil(dishData, @"UserDishBookmark");
    dish.bookmarkText = ValueOrNil(bookmarkDict, @"BookmarkText");
    dish.bookmarked = [ValueOrNil(bookmarkDict, @"Bookmarked") boolValue];
    dish.coWorkerName = ValueOrNil(bookmarkDict, @"CoWorkerName");
    dish.entityId = ValueOrNil(bookmarkDict, @"EntityId");
    dish.entityType = [self entityTypeFrom: ValueOrNil(bookmarkDict, @"EntityType")];
    dish.totalBookmarked = [ValueOrNil(bookmarkDict, @"TotalBookmarked") integerValue];
    dish.userId = ValueOrNil(bookmarkDict, @"userId");
    return dish;
}
+ (EntityType)entityTypeFrom:(NSString*)typeString
{
    if ([typeString isEqualToString: @"DISH"]) {
        return EntityTypeDish;
    }
    return EntityTypeNone;
}
- (id)copyWithZone:(NSZone *)zone
{
    Dish *dish = [Dish new];
    dish.categoryId = self.categoryId;
    dish.categoryName = self.categoryName;
    dish.dishDesc = self.dishDesc;
    dish.dishHasImage = self.dishHasImage;
    dish.dishId = self.dishId;
    dish.dishImageURL = self.dishImageURL;
    dish.dishName = self.dishName;
    dish.dishPrice = self.dishPrice;
    dish.minimalQuantity = self.minimalQuantity;
    dish.thereAreNoChoices = self.thereAreNoChoices;
    dish.choices = [[NSArray alloc] initWithArray: self.choices copyItems: YES];
    dish.bookmarkText = self.bookmarkText;
    dish.bookmarked = self.bookmarked;
    dish.coWorkerName = self.coWorkerName;
    dish.entityId = self.entityId;
    dish.entityType = self.entityType;
    dish.totalBookmarked = self.totalBookmarked;
    dish.userId = self.userId;
    dish.orderIndex = self.orderIndex;
    return dish;
}
@end
