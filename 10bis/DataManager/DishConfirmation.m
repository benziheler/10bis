//
//  DishConfirmation.m
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "DishConfirmation.h"
#import "Choice.h"
@implementation DishConfirmation
+ (instancetype)dishConfirmation:(NSDictionary *)data
{
    DishConfirmation *dish = [DishConfirmation new];
    dish.dishId = ValueOrNil(data, @"ID");
    dish.dishName = ValueOrNil(data, @"DishName");
    dish.dishDesc = ValueOrNil(data, @"DishDescription");
    dish.dishNotes = ValueOrNil(data, @"DishNotes");
    
    dish.singleDishPrice = ValueOrNil(data, @"SingleDishPrice");
    dish.totalDishPrice = ValueOrNil(data, @"TotalDishViewPrice");
    dish.totalDishPriceString = ValueOrNil(data, @"TotalDishViewPriceStr");

    dish.shoppingCartIndex = [ValueOrNil(data, @"ShoppingCartDishIndex") integerValue];
    dish.orderIndex = [ValueOrNil(data, @"OrderIndex") integerValue];
    dish.quantity = [ValueOrNil(data, @"Quantity") integerValue];
    
    dish.dishOwnerId = ValueOrNil(data, @"DishOwnerId");
    dish.dishUserId = ValueOrNil(data, @"DishUserId");
    dish.dishUsername = ValueOrNil(data, @"DishUserName");
    
    NSArray *choicesRaw = ValueOrNil(data, @"Choices");
    NSMutableArray *choices = [NSMutableArray arrayWithCapacity: choicesRaw.count];
    for (NSDictionary *choiceRaw in choicesRaw)
    {
        Choice *choice = [Choice choiceWithData: choiceRaw];
        [choices addObject: choice];
    }
    dish.choices = choices;
    return dish;
}
@end
