//
//  Street.m
//  10bis
//
//  Created by Vadim Pavlov on 23.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "Street.h"

@implementation Street
+ (id)streetWithData:(NSDictionary*)streetData
{
    Street *street = [Street new];
    street.streetId = ValueOrNil(streetData, @"Id");
    street.streetName = ValueOrNil(streetData, @"Name");
    return street;
}

@end
