//
//  Dish.h
//  10bis
//
//  Created by Vadim Pavlov on 28.10.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, EntityType)
{
    EntityTypeNone,
    EntityTypeDish,
};
@interface Dish : NSObject <NSCopying>
@property (nonatomic, strong) NSNumber *categoryId;
@property (nonatomic, strong) NSString *categoryName;

@property (nonatomic, strong) NSNumber *dishId;
@property (nonatomic, strong) NSString *dishDesc;
@property (nonatomic, strong) NSString *dishName;

@property (nonatomic, strong) NSArray *choices;

@property (nonatomic, assign) BOOL dishHasImage;
@property (nonatomic, strong) NSString *dishImageURL;

@property (nonatomic, strong) NSString *dishPrice;
@property (nonatomic, assign) NSInteger minimalQuantity;
@property (nonatomic, assign) BOOL thereAreNoChoices;

@property (nonatomic, strong) NSString *bookmarkText;
@property (nonatomic, assign) BOOL bookmarked;
@property (nonatomic, strong) NSString *coWorkerName;
@property (nonatomic, strong) NSNumber *entityId;
@property (nonatomic, assign) EntityType entityType;
@property (nonatomic, assign) NSInteger totalBookmarked;
@property (nonatomic, strong) NSNumber *userId;

@property (nonatomic, assign) NSInteger orderIndex;
+ (id)dishWithData:(NSDictionary*)dishData;
@end
