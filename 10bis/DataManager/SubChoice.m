//
//  SubChoice.m
//  10bis
//
//  Created by Vadim Pavlov on 13.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import "SubChoice.h"

@implementation SubChoice
+ (instancetype)subChoiceWithData:(NSDictionary*)data
{
    SubChoice *sub = [SubChoice new];
    sub.ID = ValueOrNil(data, @"ID");
    sub.name = ValueOrNil(data, @"Name");
    sub.price = ValueOrNil(data, @"Price");
    return sub;
}
- (id)copyWithZone:(NSZone *)zone
{
    SubChoice *subChoice = [SubChoice new];
    subChoice.ID = self.ID;
    subChoice.name = self.name;
    subChoice.price = self.price;
    return subChoice;
}
@end
