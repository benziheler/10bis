//
//  OrderConfirmation.m
//  10bis
//
//  Created by Vadim Pavlov on 13.01.14.
//  Copyright (c) 2014 OnO Apps. All rights reserved.
//

#import "OrderConfirmation.h"
#import "DishConfirmation.h"
#import "BillingLine.h"
@implementation OrderConfirmation
+ (instancetype)orderConfirmationWithData:(NSDictionary*)data
{
    OrderConfirmation *order = [OrderConfirmation new];
    order.showTipSelection = [ValueOrNil(data, @"ShowTipSelection") boolValue];
    order.restaurantId = ValueOrNil(data, @"ResId");
    
    NSArray *dishList = ValueOrNil(data, @"DishList");
    NSMutableArray *dishes = [NSMutableArray arrayWithCapacity: dishList.count];
    for (NSDictionary *dishData in dishList)
    {
        DishConfirmation *dish = [DishConfirmation dishConfirmation: dishData];
        [dishes addObject: dish];
    }
    order.dishes = dishes;
    
    NSArray *billingLinesData = ValueOrNil(data, @"BillingLines");
    NSMutableArray *billingLines = [NSMutableArray arrayWithCapacity: billingLinesData.count];
    for (NSDictionary *billingData in billingLinesData)
    {
        BillingLine *line = [BillingLine billingTypeFromData: billingData];
        [billingLines addObject: line];
    }
    order.billingLines = billingLines;
    
    order.tipValues = ValueOrNil(data, @"TipValues");
    return order;
}
@end
