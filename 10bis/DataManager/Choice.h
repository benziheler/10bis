//
//  Choice.h
//  10bis
//
//  Created by Vadim Pavlov on 12.12.13.
//  Copyright (c) 2013 OnO Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, ChoiceType)
{
    ChoiceTypeUnknown,
    ChoiceTypeComboBox,
    ChoiceTypeCheckBox,
    ChoiceTypeBeverages,
    ChoiceTypeRadioButtons,
    ChoiceTypeNotes,
};

@interface Choice : NSObject <NSCopying>
@property (nonatomic, assign) NSInteger choicePosition;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, assign) NSInteger maxPicksAllowed;
@property (nonatomic, strong) NSArray* subsChosen;
@property (nonatomic, strong) NSString *textBeforeChoosing;
@property (nonatomic, assign) ChoiceType type;

+ (instancetype)choiceWithData:(NSDictionary*)choiceData;

@end
